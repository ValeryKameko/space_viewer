#include "renderers/point_renderer.h"
#include "materials/color_material.h"
#include <QOpenGLFunctions>

PointRenderer::PointRenderer()
    : Renderer()
    , m_program()
    , m_point_size(1.0) {

}

PointRenderer::~PointRenderer() {

}

void PointRenderer::initialize() {
    m_context->functions()->glEnable(GL_PROGRAM_POINT_SIZE);

    m_program.addShaderFromSourceFile(
                QOpenGLShader::Vertex,
                "./resources/shaders/point_shader.vert");
    m_program.addShaderFromSourceFile(
                QOpenGLShader::Fragment,
                "./resources/shaders/point_shader.frag");
    if (!m_program.link()) {
        qDebug() << m_program.log() << "\n";
    }
}

void PointRenderer::render() {
    if (!m_model || !m_model->material())
        return;
    m_program.bind();

    const QSharedPointer<Material> &material = model()->material();
    const QColor &color = material.dynamicCast<ColorMaterial>()->color();

    m_program.setUniformValue("matrix",
                              m_camera->projection_matrix() *
                              m_camera->transformation_matrix().inverted());
    m_program.setUniformValue("color", color);

    m_program.enableAttributeArray("vertex_position");
    m_program.setUniformValue("point_size", m_point_size);

    m_context->functions()->glEnable(GL_POINT_SMOOTH);
    m_context->functions()->glEnable(GL_BLEND);
    m_context->functions()->glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    renderMesh(model()->mesh());

    m_program.disableAttributeArray("vertex_position");
    m_program.release();
}

void PointRenderer::uninitialize() {

}

void PointRenderer::renderMesh(const QSharedPointer<Mesh> &mesh) {
    if (!mesh.dynamicCast<CompositeMesh>().isNull())
        renderCompositeMesh(mesh.dynamicCast<CompositeMesh>());
    else if (!mesh.dynamicCast<LineMesh>().isNull())
        renderLineMesh(mesh.dynamicCast<LineMesh>());
    else if (!mesh.dynamicCast<PolygonMesh>().isNull())
        renderPolygonMesh(mesh.dynamicCast<PolygonMesh>());
    else if (!mesh.dynamicCast<PointMesh>().isNull())
        renderPointMesh(mesh.dynamicCast<PointMesh>());
}

void PointRenderer::renderLineMesh(const QSharedPointer<LineMesh> &mesh) {
    m_program.setAttributeArray("vertex_position", mesh->vertices().constData());
    m_context->functions()->glDrawArrays(GL_POINTS, 0, mesh->vertices().size());
}

void PointRenderer::renderCompositeMesh(const QSharedPointer<CompositeMesh> &mesh) {
    for (const QSharedPointer<Mesh> &sub_mesh : mesh->meshes()) {
        renderMesh(sub_mesh);
    }
}

void PointRenderer::renderPolygonMesh(const QSharedPointer<PolygonMesh> &mesh) {
    m_program.setAttributeArray("vertex_position", mesh->vertices().constData());
    m_context->functions()->glDrawArrays(GL_POINTS, 0, mesh->vertices().size());
}

void PointRenderer::renderPointMesh(const QSharedPointer<PointMesh> &mesh) {
    m_program.setAttributeArray("vertex_position", &mesh->vertex());
    m_context->functions()->glDrawArrays(GL_POINTS, 0, 1);
}

const float &PointRenderer::point_size() const {
    return m_point_size;
}

float &PointRenderer::point_size() {
    return m_point_size;
}

#include "renderers/polygon_renderer.h"
#include "materials/color_material.h"
#include "renderer.h"
#include "model.h"
#include "meshes/line_mesh.h"
#include "meshes/polygon_mesh.h"
#include <QOpenGLShaderProgram>
#include <QOpenGLContext>
#include <QOpenGLFunctions>
#include <QDir>

PolygonRenderer::PolygonRenderer()
    : Renderer() {

}

PolygonRenderer::~PolygonRenderer() {

}

void PolygonRenderer::initialize() {
    m_program.addShaderFromSourceFile(
                QOpenGLShader::Vertex,
                "./resources/shaders/polygon_shader.vert");
    m_program.addShaderFromSourceFile(
                QOpenGLShader::Fragment,
                "./resources/shaders/polygon_shader.frag");
    if (!m_program.link()) {
        qDebug() << m_program.log() << "\n";
    }
}

void PolygonRenderer::render() {
    if (!m_model || !m_model->material())
        return;
    m_program.bind();

    const QSharedPointer<Material> &material = model()->material();
    const QColor &color = material.dynamicCast<ColorMaterial>()->color();

    m_program.setUniformValue("matrix",
                              m_camera->projection_matrix() *
                              m_camera->transformation_matrix().inverted());
    m_program.setUniformValue("color", color);

    m_program.enableAttributeArray("vertex_position");

    m_context->functions()->glEnable(GL_BLEND);
    m_context->functions()->glEnable(GL_POLYGON_SMOOTH);
    m_context->functions()->glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    renderMesh(model()->mesh());

    m_program.disableAttributeArray("vertex_position");
    m_program.release();
}

void PolygonRenderer::uninitialize() {

}

void PolygonRenderer::renderMesh(const QSharedPointer<Mesh> &mesh) {
    if (!mesh.dynamicCast<CompositeMesh>().isNull())
        renderCompositeMesh(mesh.dynamicCast<CompositeMesh>());
    else if (!mesh.dynamicCast<PolygonMesh>().isNull())
        renderPolygonMesh(mesh.dynamicCast<PolygonMesh>());
}

void PolygonRenderer::renderCompositeMesh(const QSharedPointer<CompositeMesh> &mesh) {
    for (const QSharedPointer<Mesh> &sub_mesh : mesh->meshes())
        renderMesh(sub_mesh);
}

void PolygonRenderer::renderPolygonMesh(const QSharedPointer<PolygonMesh> &mesh) {
    m_program.setAttributeArray("vertex_position", mesh->vertices().constData());
    m_context->functions()->glDrawArrays(GL_POLYGON, 0, mesh->vertices().size());
}

#include "parser.h"
#include "ast_nodes/ast_constant_node.h"
#include "ast_nodes/ast_function_node.h"
#include "ast_nodes/ast_variable_node.h"
#include <QMap>
#include <QVector>

enum class OperatorAssociativity {
    Left, Right
};

const QVector<QPair<QString, OperatorAssociativity>> operators = {
    qMakePair(QString("+"), OperatorAssociativity::Left),
    qMakePair(QString("-"), OperatorAssociativity::Right),
    qMakePair(QString("*"), OperatorAssociativity::Left),
    qMakePair(QString("/"), OperatorAssociativity::Right),
    qMakePair(QString("^"), OperatorAssociativity::Right),
};

Parser::Parser() {

}

Parser::~Parser() {

}

QSharedPointer<ASTNode> Parser::parse(const QString &_function) {
    QString function = _function.trimmed();
    if (function == "")
        return nullptr;
    QMap<QString, int> operator_positions;
    int enclosing_delta = 0;

    for (int i = 0; i < function.size(); i++) {
        QStringRef suffix = function.rightRef(function.size() - i);
        if (suffix.startsWith("(")) {
            enclosing_delta++;
            continue;
        } else if (suffix.startsWith(")")) {
            enclosing_delta--;
            continue;
        }
        if (suffix.front() == QChar(' '))
            continue;

        if (enclosing_delta < 0)
            return nullptr;

        if (enclosing_delta == 0) {
            if (i == 0)
                continue;
            for (const QPair<QString, OperatorAssociativity> &op : operators) {
                if (!suffix.startsWith(op.first))
                    continue;
                if (operator_positions.contains(op.first)) {
                    if (op.second == OperatorAssociativity::Right)
                        operator_positions[op.first] = i;
                } else {
                    operator_positions[op.first] = i;
                }
            }
        }
    }
    if (enclosing_delta != 0)
        return nullptr;

    if (operator_positions.contains("+")) {
        int operator_position = operator_positions["+"];
        QString left = function.left(operator_position);
        QString right = function.right(function.length() - operator_position - 1);

        QSharedPointer<ASTFunctionNode> node(new ASTFunctionNode());
        QSharedPointer<ASTNode> left_node = parse(left);
        QSharedPointer<ASTNode> right_node = parse(right);
        if (!left_node || !right_node)
            return nullptr;
        node->parameters().append(left_node);
        node->parameters().append(right_node);
        node->function_name() = "sum";
        return node;
    } else if (operator_positions.contains("-")) {
        int operator_position = operator_positions["-"];
        QString left = function.left(operator_position);
        QString right = function.right(function.length() - operator_position - 1);

        QSharedPointer<ASTFunctionNode> node(new ASTFunctionNode());
        QSharedPointer<ASTNode> left_node = parse(left);
        QSharedPointer<ASTNode> right_node = parse(right);
        if (!left_node || !right_node)
            return nullptr;
        node->parameters().append(left_node);
        node->parameters().append(right_node);
        node->function_name() = "subtract";
        return node;
    } else if (function[0] == "+") {
        int operator_position = 0;
        QString right = function.right(function.length() - operator_position - 1);

        QSharedPointer<ASTFunctionNode> node(new ASTFunctionNode());
        QSharedPointer<ASTNode> right_node = parse(right);
        if (!right_node)
            return nullptr;
        node->parameters().append(right_node);
        node->function_name() = "unary_plus";
        return node;
    } else if (function[0] == "-") {
        int operator_position = 0;
        QString right = function.right(function.length() - operator_position - 1);

        QSharedPointer<ASTFunctionNode> node(new ASTFunctionNode());
        QSharedPointer<ASTNode> right_node = parse(right);
        if (!right_node)
            return nullptr;
        node->parameters().append(right_node);
        node->function_name() = "unary_minus";
        return node;
    } else if (operator_positions.contains("*")) {
        int operator_position = operator_positions["*"];
        QString left = function.left(operator_position);
        QString right = function.right(function.length() - operator_position - 1);

        QSharedPointer<ASTFunctionNode> node(new ASTFunctionNode());
        QSharedPointer<ASTNode> left_node = parse(left);
        QSharedPointer<ASTNode> right_node = parse(right);
        if (!left_node || !right_node)
            return nullptr;
        node->parameters().append(left_node);
        node->parameters().append(right_node);
        node->function_name() = "multiply";
        return node;
    } else if (operator_positions.contains("/")) {
        int operator_position = operator_positions["/"];
        QString left = function.left(operator_position);
        QString right = function.right(function.length() - operator_position - 1);

        QSharedPointer<ASTFunctionNode> node(new ASTFunctionNode());
        QSharedPointer<ASTNode> left_node = parse(left);
        QSharedPointer<ASTNode> right_node = parse(right);
        if (!left_node || !right_node)
            return nullptr;
        node->parameters().append(left_node);
        node->parameters().append(right_node);
        node->function_name() = "divide";
        return node;
    } else if (operator_positions.contains("^")) {
        int operator_position = operator_positions["^"];
        QString left = function.left(operator_position);
        QString right = function.right(function.length() - operator_position - 1);

        QSharedPointer<ASTFunctionNode> node(new ASTFunctionNode());
        QSharedPointer<ASTNode> left_node = parse(left);
        QSharedPointer<ASTNode> right_node = parse(right);
        if (!left_node || !right_node)
            return nullptr;
        node->parameters().append(left_node);
        node->parameters().append(right_node);
        node->function_name() = "power";
        return node;
    }

    bool is_constant = false;
    float value = function.toFloat(&is_constant);
    if (is_constant) {
        QSharedPointer<ASTConstantNode> node(new ASTConstantNode());
        node->value() = value;
        return node;
    }

    if (!function.contains("(") || !function.contains(")")) {
        QSharedPointer<ASTVariableNode> node(new ASTVariableNode());
        node->variable_name() = function;
        return node;
    }

    int bracket_position = function.indexOf("(");
    QString function_name = function.left(bracket_position);
    if (function.right(1) != ")")
        return nullptr;

    QString argument = function.mid(bracket_position + 1, function.length() - 2 - function_name.length());
    QSharedPointer<ASTNode> argument_node = parse(argument);
    if (!argument_node)
        return nullptr;
    if (function_name == "")
        return argument_node;

    QSharedPointer<ASTFunctionNode> node(new ASTFunctionNode());
    node->function_name() = function_name;
    node->parameters().append(argument_node);
    return node;
}

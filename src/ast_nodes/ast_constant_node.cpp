#include "ast_nodes/ast_constant_node.h"

ASTConstantNode::ASTConstantNode(float value)
    : ASTNode()
    , m_value(value) {

}

ASTConstantNode::~ASTConstantNode() {

}

const float &ASTConstantNode::value() const {
    return m_value;
}

float &ASTConstantNode::value() {
    return m_value;
}

float ASTConstantNode::evaluate(const QMap<QString, float> &values) const {
    return m_value;
}

#include "ast_nodes/ast_function_node.h"
#include <QtMath>
#include <limits>

ASTFunctionNode::ASTFunctionNode()
    : ASTNode()
    , m_function_name("")
    , m_parameters() {

}

ASTFunctionNode::~ASTFunctionNode() {

}

const QString &ASTFunctionNode::function_name() const {
    return m_function_name;
}

QString &ASTFunctionNode::function_name() {
    return m_function_name;
}

const QVector<QSharedPointer<ASTNode>> &ASTFunctionNode::parameters() const {
    return m_parameters;
}

QVector<QSharedPointer<ASTNode>> &ASTFunctionNode::parameters() {
    return m_parameters;
}

float ASTFunctionNode::evaluate(const QMap<QString, float> &values) const {
    QVector<float> parameter_values;
    for (const QSharedPointer<ASTNode> &parameter : m_parameters) {
        float parameter_value = parameter->evaluate(values);
        if (std::isnan(parameter_value))
            return std::numeric_limits<float>::quiet_NaN();
        parameter_values.append(parameter_value);
    }
    if (m_function_name == "sum") {
        if (m_parameters.size() != 2)
            return std::numeric_limits<float>::quiet_NaN();
        return parameter_values[0] + parameter_values[1];
    } else if (m_function_name == "unary_plus") {
        if (m_parameters.size() != 1)
            return std::numeric_limits<float>::quiet_NaN();
        return parameter_values[0];
    } else if (m_function_name == "unary_minus") {
        if (m_parameters.size() != 1)
            return std::numeric_limits<float>::quiet_NaN();
        return -parameter_values[0];
    } else if (m_function_name == "subtract") {
        if (m_parameters.size() != 2)
            return std::numeric_limits<float>::quiet_NaN();
        return parameter_values[0] - parameter_values[1];
    } else if (m_function_name == "multiply") {
        if (m_parameters.size() != 2)
            return std::numeric_limits<float>::quiet_NaN();
        return parameter_values[0] * parameter_values[1];
    } else if (m_function_name == "divide") {
        if (m_parameters.size() != 2)
            return std::numeric_limits<float>::quiet_NaN();
        return parameter_values[0] / parameter_values[1];
    } else if (m_function_name == "sin") {
        if (m_parameters.size() != 1)
            return std::numeric_limits<float>::quiet_NaN();
        return std::sin(parameter_values[0]);
    } else if (m_function_name == "cos") {
        if (m_parameters.size() != 1)
            return std::numeric_limits<float>::quiet_NaN();
        return std::cos(parameter_values[0]);
    } else if (m_function_name == "tan") {
        if (m_parameters.size() != 1)
            return std::numeric_limits<float>::quiet_NaN();
        return std::tan(parameter_values[0]);
    } else if (m_function_name == "power") {
        if (m_parameters.size() != 2)
            return std::numeric_limits<float>::quiet_NaN();
        return std::pow(parameter_values[0], parameter_values[1]);
    } else if (m_function_name == "sqrt") {
        if (m_parameters.size() != 1)
            return std::numeric_limits<float>::quiet_NaN();
        return std::sqrt(parameter_values[0]);
    } else if (m_function_name == "log") {
        if (m_parameters.size() != 1)
            return std::numeric_limits<float>::quiet_NaN();
        return std::log(parameter_values[0]);
    } else
        return std::numeric_limits<float>::quiet_NaN();
}

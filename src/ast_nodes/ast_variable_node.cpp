#include "ast_nodes/ast_variable_node.h"
#include <limits>

ASTVariableNode::ASTVariableNode(const QString &variable_name)
    : ASTNode()
    , m_variable_name(variable_name) {

}

ASTVariableNode::~ASTVariableNode() {

}

const QString &ASTVariableNode::variable_name() const {
    return m_variable_name;
}

QString &ASTVariableNode::variable_name() {
    return m_variable_name;
}

float ASTVariableNode::evaluate(const QMap<QString, float> &values) const {
    return values.value(m_variable_name,
                        std::numeric_limits<float>::quiet_NaN());
}

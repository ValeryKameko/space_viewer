#include "camera.h"

Camera::Camera()
    : m_projection_matrix(), m_transformation_matrix() {

}

Camera::~Camera() {

}

const QMatrix4x4 &Camera::projection_matrix() const {
    return m_projection_matrix;
}

const QMatrix4x4 &Camera::transformation_matrix() const {
    return m_transformation_matrix;
}

QMatrix4x4 &Camera::projection_matrix() {
    return m_projection_matrix;
}

QMatrix4x4 &Camera::transformation_matrix() {
    return m_transformation_matrix;
}

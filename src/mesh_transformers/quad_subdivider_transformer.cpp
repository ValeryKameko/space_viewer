#include "mesh_transformers/quad_subdivider_transformer.h"
#include <QVector>

QuadSubdividerTransformer::QuadSubdividerTransformer(unsigned int subdivision_count)
    : m_subdivision_count(subdivision_count) {

}

QuadSubdividerTransformer::~QuadSubdividerTransformer() {

}

QSharedPointer<Mesh> QuadSubdividerTransformer::transform(const QSharedPointer<Mesh> &mesh) {
    if (!mesh.dynamicCast<CompositeMesh>().isNull())
        return subdivideCompositeMesh(mesh.dynamicCast<CompositeMesh>());
    else if (!mesh.dynamicCast<QuadMesh>().isNull())
        return subdivideQuadMesh(mesh.dynamicCast<QuadMesh>());
    else
       return mesh;
}

QSharedPointer<Mesh> QuadSubdividerTransformer::subdivideCompositeMesh(const QSharedPointer<CompositeMesh> &mesh) {
    QSharedPointer<CompositeMesh> subdivided_mesh(new CompositeMesh());
    for (const QSharedPointer<Mesh> &sub_mesh : mesh->meshes())
        subdivided_mesh->meshes().append(transform(sub_mesh));
    return subdivided_mesh;
}

QSharedPointer<Mesh> QuadSubdividerTransformer::subdivideQuadMesh(const QSharedPointer<QuadMesh> &mesh)  {
    QSharedPointer<CompositeMesh> subdivided_mesh(new CompositeMesh());

    QVector<QVector<QVector3D>> point_table;

    point_table.resize(m_subdivision_count + 1);
    for (unsigned int i = 0; i <= m_subdivision_count; i++) {
        point_table[i].resize(m_subdivision_count + 1);
        for (unsigned int j = 0; j <= m_subdivision_count; j++)
            point_table[i][j] = subdivisionPoint(mesh, i, j);
    }

    for (unsigned int i = 0; i < m_subdivision_count; i++) {
        for (unsigned int j = 0; j < m_subdivision_count; j++) {
            QSharedPointer<QuadMesh> subdivision_quad(new QuadMesh());
            subdivision_quad->vertices().append(point_table[i][j]);
            subdivision_quad->vertices().append(point_table[i][j + 1]);
            subdivision_quad->vertices().append(point_table[i + 1][j + 1]);
            subdivision_quad->vertices().append(point_table[i + 1][j]);

            subdivided_mesh->meshes().append(subdivision_quad);
        }
    }
    return subdivided_mesh;
}

QVector3D QuadSubdividerTransformer::subdivisionPoint(const QSharedPointer<QuadMesh> &mesh,
                                                      unsigned int i,
                                                      unsigned int j) {
    QVector3D v0 = mesh->vertices()[0];
    QVector3D v1 = mesh->vertices()[1];
    QVector3D v2 = mesh->vertices()[2];
    QVector3D v3 = mesh->vertices()[3];

    QVector3D v01 = (v0 * i + v1 * (m_subdivision_count - i)) / m_subdivision_count;
    QVector3D v23 = (v3 * i + v2 * (m_subdivision_count - i)) / m_subdivision_count;

    QVector3D v0123 = (v01 * j + v23 * (m_subdivision_count - j)) / m_subdivision_count;
    return v0123;
}


const unsigned int &QuadSubdividerTransformer::subdivision_count() const {
    return m_subdivision_count;
}
unsigned int &QuadSubdividerTransformer::subdivision_count() {
    return m_subdivision_count;
}

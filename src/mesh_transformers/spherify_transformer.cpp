#include "mesh_transformers/spherify_transformer.h"

SpherifyTransformer::SpherifyTransformer(float radius)
    : m_radius(radius){

}

SpherifyTransformer::~SpherifyTransformer() {

}

QSharedPointer<Mesh> SpherifyTransformer::transform(const QSharedPointer<Mesh> &mesh) {
    if (!mesh.dynamicCast<CompositeMesh>().isNull())
        return spherifyCompositeMesh(mesh.dynamicCast<CompositeMesh>());
    else if (!mesh.dynamicCast<TriangleMesh>().isNull())
        return spherifyTriangleMesh(mesh.dynamicCast<TriangleMesh>());
    else if (!mesh.dynamicCast<QuadMesh>().isNull())
        return spherifyQuadMesh(mesh.dynamicCast<QuadMesh>());
    else if (!mesh.dynamicCast<PolygonMesh>().isNull())
        return spherifyPolygonMesh(mesh.dynamicCast<PolygonMesh>());
    else
       return mesh;
}

QSharedPointer<Mesh> SpherifyTransformer::spherifyTriangleMesh(const QSharedPointer<TriangleMesh> &mesh) {
    QSharedPointer<TriangleMesh> transfromed_mesh(new TriangleMesh());
    for (const QVector3D &vertex : mesh->vertices())
        transfromed_mesh->vertices().append(m_radius * vertex.normalized());
    return transfromed_mesh;
}

QSharedPointer<Mesh> SpherifyTransformer::spherifyPolygonMesh(const QSharedPointer<PolygonMesh> &mesh) {
    QSharedPointer<PolygonMesh> transformed_mesh(new PolygonMesh());
    for (const QVector3D &vertex : mesh->vertices())
        transformed_mesh->vertices().append(m_radius * vertex.normalized());
    return transformed_mesh;
}

QSharedPointer<Mesh> SpherifyTransformer::spherifyQuadMesh(const QSharedPointer<QuadMesh> &mesh) {
    QSharedPointer<QuadMesh> transformed_mesh(new QuadMesh());
    for (const QVector3D &vertex : mesh->vertices())
        transformed_mesh->vertices().append(m_radius * vertex.normalized());
    return transformed_mesh;
}

QSharedPointer<Mesh> SpherifyTransformer::spherifyCompositeMesh(const QSharedPointer<CompositeMesh> &mesh) {
    QSharedPointer<CompositeMesh> transformed_mesh(new CompositeMesh());
    for (const QSharedPointer<Mesh> &sub_mesh : mesh->meshes())
        transformed_mesh->meshes().append(transform(sub_mesh ));
    return transformed_mesh;
}

const float &SpherifyTransformer::radius() const {
    return m_radius;
}

float &SpherifyTransformer::radius() {
    return m_radius;
}

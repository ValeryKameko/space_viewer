#include "mesh_transformers/transformation_transformer.h"
#include <QVector>

TransformationTransformer::TransformationTransformer(const QMatrix4x4 &transformation)
    : m_transformation(transformation) {

}

TransformationTransformer::~TransformationTransformer() {

}

QSharedPointer<Mesh> TransformationTransformer::transform(const QSharedPointer<Mesh> &mesh) {
    if (!mesh.dynamicCast<CompositeMesh>().isNull())
        return transformCompositeMesh(mesh.dynamicCast<CompositeMesh>());
    else if (!mesh.dynamicCast<TriangleMesh>().isNull())
        return transformTriangleMesh(mesh.dynamicCast<TriangleMesh>());
    else if (!mesh.dynamicCast<QuadMesh>().isNull())
        return transformQuadMesh(mesh.dynamicCast<QuadMesh>());
    else if (!mesh.dynamicCast<PolygonMesh>().isNull())
        return transformPolygonMesh(mesh.dynamicCast<PolygonMesh>());
    else
       return mesh;
}

QSharedPointer<Mesh> TransformationTransformer::transformTriangleMesh(const QSharedPointer<TriangleMesh> &mesh) {
    QSharedPointer<TriangleMesh> transfromed_mesh(new TriangleMesh());
    for (const QVector3D &vertex : mesh->vertices())
        transfromed_mesh->vertices().append(m_transformation * vertex);
    return transfromed_mesh;
}

QSharedPointer<Mesh> TransformationTransformer::transformPolygonMesh(const QSharedPointer<PolygonMesh> &mesh) {
    QSharedPointer<PolygonMesh> transfromed_mesh(new PolygonMesh());
    for (const QVector3D &vertex : mesh->vertices())
        transfromed_mesh->vertices().append(m_transformation * vertex);
    return transfromed_mesh;
}

QSharedPointer<Mesh> TransformationTransformer::transformQuadMesh(const QSharedPointer<QuadMesh> &mesh) {
    QSharedPointer<QuadMesh> transfromed_mesh(new QuadMesh());
    for (const QVector3D &vertex : mesh->vertices())
        transfromed_mesh->vertices().append(m_transformation * vertex);
    return transfromed_mesh;
}

QSharedPointer<Mesh> TransformationTransformer::transformCompositeMesh(const QSharedPointer<CompositeMesh> &mesh) {
    QSharedPointer<CompositeMesh> transfromed_mesh(new CompositeMesh());
    for (const QSharedPointer<Mesh> &sub_mesh : mesh->meshes())
        transfromed_mesh->meshes().append(transform(sub_mesh ));
    return transfromed_mesh;
}

const QMatrix4x4 &TransformationTransformer::transformation() const {
    return m_transformation;
}
QMatrix4x4 &TransformationTransformer::transformation() {
    return m_transformation;
}

#include "mesh_transformers/triangle_subdivider_transformer.h"

TriangleSubdividerTransformer::TriangleSubdividerTransformer(unsigned int subdivision_level)
    : m_subdivision_level(subdivision_level) {

}

TriangleSubdividerTransformer::~TriangleSubdividerTransformer() {

}

QSharedPointer<Mesh> TriangleSubdividerTransformer::transform(const QSharedPointer<Mesh> &mesh) {
    if (!mesh.dynamicCast<CompositeMesh>().isNull())
        return subdivideCompositeMesh(mesh.dynamicCast<CompositeMesh>());
    else if (!mesh.dynamicCast<TriangleMesh>().isNull())
        return subdivideTriangleMesh(mesh.dynamicCast<TriangleMesh>());
    else
       return mesh;
}

QSharedPointer<Mesh> TriangleSubdividerTransformer::subdivideCompositeMesh(const QSharedPointer<CompositeMesh> &mesh) {
    QSharedPointer<CompositeMesh> subdivided_mesh(new CompositeMesh());
    for (const QSharedPointer<Mesh> &sub_mesh : mesh->meshes())
        subdivided_mesh->meshes().append(transform(sub_mesh));
    return subdivided_mesh;
}

QSharedPointer<Mesh> TriangleSubdividerTransformer::subdivideTriangleMesh(const QSharedPointer<TriangleMesh> &mesh)  {
    QSharedPointer<CompositeMesh> subdivided_mesh(new CompositeMesh());

    QVector3D v0 = mesh->vertices()[0];
    QVector3D v1 = mesh->vertices()[1];
    QVector3D v2 = mesh->vertices()[2];

    QVector<QVector<QVector3D>> vertices;
    vertices.resize(m_subdivision_level + 1);
    for (unsigned int i = 0; i <= m_subdivision_level; i++) {
        vertices[i].resize(i + 1);

        QVector3D left_point  = (v1 * i + v0 * (m_subdivision_level - i)) / (m_subdivision_level);
        QVector3D right_point = (v2 * i + v0 * (m_subdivision_level - i)) / (m_subdivision_level);
        if (i == 0) {
            vertices[0][0] = v0;
        } else {
            for (unsigned int j = 0; j <= i; j++) {
                vertices[i][j] = (left_point * j + right_point * (i - j)) / i;
            }
        }
    }

    for (unsigned int i = 0; i < m_subdivision_level; i++) {
        for (unsigned int j = 0; j <= i; j++) {
            QSharedPointer<TriangleMesh> face(new TriangleMesh());
            face->vertices().append(vertices[i][j]);
            face->vertices().append(vertices[i + 1][j]);
            face->vertices().append(vertices[i + 1][j + 1]);

            subdivided_mesh->meshes().append(face);
        }
    }

    for (unsigned int i = 1; i <= m_subdivision_level; i++) {
        for (unsigned int j = 1; j + 1 <= i; j++) {
            QSharedPointer<TriangleMesh> face(new TriangleMesh());
            face->vertices().append(vertices[i][j]);
            face->vertices().append(vertices[i - 1][j - 1]);
            face->vertices().append(vertices[i - 1][j]);

            subdivided_mesh->meshes().append(face);
        }
    }

    return subdivided_mesh;
}

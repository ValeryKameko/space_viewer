#include "mesh_functions/foreach_vertex_function.h"

ForeachVertexFunction::ForeachVertexFunction()
    : MeshFunction() {

}

ForeachVertexFunction::~ForeachVertexFunction() {

}

void ForeachVertexFunction::run(const QSharedPointer<Mesh> &mesh, const std::function<void(const QVector3D &vertex)> &func) {
    if (!mesh.dynamicCast<CompositeMesh>().isNull())
        runCompositeMesh(mesh.dynamicCast<CompositeMesh>(), func);
    else if (!mesh.dynamicCast<PolygonMesh>().isNull())
        runPolygonMesh(mesh.dynamicCast<PolygonMesh>(), func);
}


void ForeachVertexFunction::runCompositeMesh(const QSharedPointer<CompositeMesh> &mesh, const std::function<void(const QVector3D &vertex)> &func) {
    for (const QSharedPointer<Mesh> &sub_mesh : mesh->meshes())
        run(sub_mesh, func);
}

void ForeachVertexFunction::runPolygonMesh(const QSharedPointer<PolygonMesh> &mesh, const std::function<void(const QVector3D &vertex)> &func) {
    for (const QVector3D &vertex : mesh->vertices())
        func(vertex);
}

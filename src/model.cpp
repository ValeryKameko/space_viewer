#include "model.h"

Model::Model()
    : m_mesh(nullptr), m_material(nullptr) {
}

Model::~Model() {

}

const QSharedPointer<Mesh> &Model::mesh() const {
    return m_mesh;
}

const QSharedPointer<Material> &Model::material() const {
    return m_material;
}

QSharedPointer<Mesh> &Model::mesh() {
    return m_mesh;
}

QSharedPointer<Material> &Model::material() {
    return m_material;
}

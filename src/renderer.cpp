#include "renderer.h"

Renderer::Renderer()
    : m_context(nullptr), m_camera(nullptr), m_model_matrix(), m_model() {

}

Renderer::~Renderer() {

}

void Renderer::setContext(QOpenGLContext * context) {
    m_context = context;
}

QOpenGLContext * Renderer::context() {
    return m_context;
}

const QSharedPointer<Camera> &Renderer::camera() const {
    return m_camera;
}

const QMatrix4x4 &Renderer::model_matrix() const {
    return m_model_matrix;
}

QSharedPointer<Camera> &Renderer::camera() {
    return m_camera;
}

QMatrix4x4 &Renderer::model_matrix() {
    return m_model_matrix;
}

const QSharedPointer<Model> &Renderer::model() const {
    return m_model;
}

QSharedPointer<Model> &Renderer::model() {
    return m_model;
}

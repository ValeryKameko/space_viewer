#include "meshes/point_mesh.h"

PointMesh::PointMesh()
    : m_vertex() {

}

PointMesh::~PointMesh() {

}

const QVector3D &PointMesh::vertex() const {
    return m_vertex;
}

QVector3D &PointMesh::vertex() {
    return m_vertex;
}

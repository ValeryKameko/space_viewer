#include "meshes/line_mesh.h"

LineMesh::LineMesh()
    : Mesh()
    , m_vertices()
        {

}

LineMesh::~LineMesh() {

}

const QVector<QVector3D> &LineMesh::vertices() const {
    return m_vertices;
}

QVector<QVector3D> &LineMesh::vertices() {
    return m_vertices;
}

const size_t &LineMesh::line_count() const {
    return m_line_count;
}

size_t &LineMesh::line_count() {
    return m_line_count;
}

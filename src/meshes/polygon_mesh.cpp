#include "meshes/polygon_mesh.h"

PolygonMesh::PolygonMesh()
    : Mesh()
    , m_vertices()
    , m_normals()
    , m_texture_coordinates()
        {

}

PolygonMesh::~PolygonMesh() {

}

const QVector<QVector3D> &PolygonMesh::vertices() const {
    return m_vertices;
}

QVector<QVector3D> &PolygonMesh::vertices() {
    return m_vertices;
}

const QVector<QVector2D> &PolygonMesh::texture_coordinates()const {
    return m_texture_coordinates;
}

QVector<QVector2D> &PolygonMesh::texture_coordinates() {
    return m_texture_coordinates;
}

const QVector<QVector3D> &PolygonMesh::normals() const {
    return m_normals;
}

QVector<QVector3D> &PolygonMesh::normals() {
    return m_normals;
}

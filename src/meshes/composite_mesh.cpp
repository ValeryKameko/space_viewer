#include "meshes/composite_mesh.h"

CompositeMesh::CompositeMesh()
    : Mesh()
    , m_meshes()
        {

}

CompositeMesh::~CompositeMesh() {

}

const QVector<QSharedPointer<Mesh>> &CompositeMesh::meshes() const {
    return m_meshes;
}

QVector<QSharedPointer<Mesh>> &CompositeMesh::meshes() {
    return m_meshes;
}

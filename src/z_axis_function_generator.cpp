#include "mesh_generators/z_axis_function_generator.h"
#include <QtMath>
#include <meshes/composite_mesh.h>
#include <meshes/quad_mesh.h>

ZAxisFunctionGenerator::ZAxisFunctionGenerator() {

}

ZAxisFunctionGenerator::~ZAxisFunctionGenerator() {

}


QSharedPointer<Mesh> ZAxisFunctionGenerator::generate() {
    if (!m_function)
        return nullptr;

    QVector<QVector<QVector3D>> arr;

    arr.resize(m_x_subdivision_level + 1);

    for (unsigned int i = 0; i <= m_x_subdivision_level; i++) {
        arr[i].resize(m_y_subdivision_level + 1);
        for (unsigned int j = 0; j <= m_y_subdivision_level; j++) {
            float x = m_min_x + (m_max_x - m_min_x) * i / (float)m_x_subdivision_level;
            float y = m_min_y + (m_max_y - m_min_y) * j / (float)m_y_subdivision_level;

            float z = m_function->evaluate(QMap<QString, float>{
                                               {QString("x"), x},
                                               {QString("y"), y}});
            if (std::isnan(z))
                return nullptr;
            arr[i][j] = QVector3D(x, y, z);
        }
    }

    QSharedPointer<CompositeMesh> mesh(new CompositeMesh());

    for (int i = 0; i < m_x_subdivision_level; i++) {
        for (int j = 0; j < m_y_subdivision_level; j++) {
            QSharedPointer<QuadMesh> polygon(new QuadMesh());
            polygon->vertices().append(arr[i][j]);
            polygon->vertices().append(arr[i + 1][j]);
            polygon->vertices().append(arr[i + 1][j + 1]);
            polygon->vertices().append(arr[i][j + 1]);

            mesh->meshes().append(polygon);
        }
    }

    return mesh;
}

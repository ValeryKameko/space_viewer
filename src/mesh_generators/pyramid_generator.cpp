#include "mesh_generators/pyramid_generator.h"
#include "meshes/composite_mesh.h"
#include "meshes/triangle_mesh.h"
#include "mesh_transformers/transformation_transformer.h"
#include <algorithm>
#include <QtMath>
#include <QQuaternion>

PyramidGenerator::PyramidGenerator(unsigned int count, float height, float radius)
    : m_count(count)
    , m_height(height)
    , m_radius(radius) {

}

PyramidGenerator::~PyramidGenerator() {

}

QSharedPointer<Mesh> PyramidGenerator::generate() {
    QSharedPointer<CompositeMesh> mesh(new CompositeMesh());

    QVector3D v0(0.0, m_height, 0.0);
    QSharedPointer<PolygonMesh> down_face(new PolygonMesh());

    for (unsigned int i = 0; i < m_count; i++) {
        float left_angle = 2 * M_PI * i / (float)m_count;
        float right_angle = 2 * M_PI * (i + 1) / (float)m_count;


        QSharedPointer<TriangleMesh> side_face(new TriangleMesh());
        side_face->vertices().append(generateVertex(left_angle));
        side_face->vertices().append(v0);
        side_face->vertices().append(generateVertex(right_angle));

        mesh->meshes().append(side_face);

        down_face->vertices().append(generateVertex(left_angle));
    }

    mesh->meshes().append(down_face);

    TransformationTransformer transformer;
    float dy = (m_height * m_height - m_radius * m_radius) / (2 * m_height);
    dy = std::min(dy, 0.0f);

    QMatrix4x4 transformation_matrix;
    transformation_matrix.translate(QVector3D(0.0, -dy, 0.0));
    transformer.transformation() = transformation_matrix;

    return transformer.transform(mesh);
}

QVector3D PyramidGenerator::generateVertex(float angle) {
    float angle_degrees = qRadiansToDegrees(angle);
    QVector3D vertex(m_radius, 0.0, 0.0);
    vertex = QQuaternion::fromAxisAndAngle(QVector3D(0.0, 1.0, 0.0), angle_degrees).rotatedVector(vertex);

    return vertex;
}

const float &PyramidGenerator::radius() const {
    return m_radius;
}

float &PyramidGenerator::radius() {
    return m_radius;
}

const float &PyramidGenerator::height() const {
    return m_height;
}

float &PyramidGenerator::height() {
    return m_height;
}

const unsigned int &PyramidGenerator::count() const {
    return m_count;
}

unsigned int &PyramidGenerator::count() {
    return m_count;
}

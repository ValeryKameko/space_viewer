#include "mesh_generators/parametrized_surface_generator.h"

#include <meshes/composite_mesh.h>
#include <meshes/quad_mesh.h>



ParametrizedSurfaceGenerator::ParametrizedSurfaceGenerator()
    : MeshGenerator()
    , m_min_s(0)
    , m_max_s(0)
    , m_min_t(0)
    , m_max_t(0)
    , m_s_subdivision_level(1)
    , m_t_subdivision_level(1)
    , m_function_x(nullptr)
    , m_function_y(nullptr)
    , m_function_z(nullptr) {
}

ParametrizedSurfaceGenerator::~ParametrizedSurfaceGenerator() {

}

QSharedPointer<Mesh> ParametrizedSurfaceGenerator::generate() {
    if (!m_function_x || !m_function_y || !m_function_z)
        return nullptr;

    QVector<QVector<QVector3D>> arr;

    arr.resize(m_s_subdivision_level + 1);

    for (unsigned int i = 0; i <= m_s_subdivision_level; i++) {
        arr[i].resize(m_t_subdivision_level + 1);
        for (unsigned int j = 0; j <= m_t_subdivision_level; j++) {
            float s = m_min_s + (m_max_s - m_min_s) * i / (float)m_s_subdivision_level;
            float t = m_min_t + (m_max_t - m_min_t) * j / (float)m_t_subdivision_level;


            float x = m_function_x->evaluate(QMap<QString, float>{
                                               {QString("s"), s},
                                               {QString("t"), t}});
            float y = m_function_y->evaluate(QMap<QString, float>{
                                               {QString("s"), s},
                                               {QString("t"), t}});
            float z = m_function_z->evaluate(QMap<QString, float>{
                                               {QString("s"), s},
                                               {QString("t"), t}});
            if (std::isnan(x) || std::isnan(y) || std::isnan(z))
                return nullptr;
            arr[i][j] = QVector3D(x, y, z);
        }
    }

    QSharedPointer<CompositeMesh> mesh(new CompositeMesh());

    for (int i = 0; i < m_s_subdivision_level; i++) {
        for (int j = 0; j < m_t_subdivision_level; j++) {
            QSharedPointer<QuadMesh> polygon(new QuadMesh());
            polygon->vertices().append(arr[i][j]);
            polygon->vertices().append(arr[i + 1][j]);
            polygon->vertices().append(arr[i + 1][j + 1]);
            polygon->vertices().append(arr[i][j + 1]);

            mesh->meshes().append(polygon);
        }
    }

    return mesh;
}

QSharedPointer<ASTNode> &ParametrizedSurfaceGenerator::function_x() {
    return m_function_x;
}

const QSharedPointer<ASTNode> &ParametrizedSurfaceGenerator::function_x() const {
    return m_function_x;
}

QSharedPointer<ASTNode> &ParametrizedSurfaceGenerator::function_y() {
    return m_function_y;
}

const QSharedPointer<ASTNode> &ParametrizedSurfaceGenerator::function_y() const {
    return m_function_y;
}

QSharedPointer<ASTNode> &ParametrizedSurfaceGenerator::function_z() {
    return m_function_z;
}

const QSharedPointer<ASTNode> &ParametrizedSurfaceGenerator::function_z() const {
    return m_function_z;
}

unsigned int &ParametrizedSurfaceGenerator::s_subdivision_level() {
    return m_s_subdivision_level;
}

const unsigned int &ParametrizedSurfaceGenerator::s_subdivision_level() const {
    return m_s_subdivision_level;
}

unsigned int &ParametrizedSurfaceGenerator::t_subdivision_level() {
    return m_t_subdivision_level;
}

const unsigned int &ParametrizedSurfaceGenerator::t_subdivision_level() const {
    return m_t_subdivision_level;
}

float &ParametrizedSurfaceGenerator::min_s() {
    return m_min_s;
}

float &ParametrizedSurfaceGenerator::max_s() {
    return m_max_s;
}

float &ParametrizedSurfaceGenerator::min_t() {
    return m_min_t;
}

float &ParametrizedSurfaceGenerator::max_t() {
    return m_max_t;
}

const float &ParametrizedSurfaceGenerator::min_s() const {
    return m_min_s;
}

const float &ParametrizedSurfaceGenerator::max_s() const {
    return m_max_s;
}

const float &ParametrizedSurfaceGenerator::min_t() const {
    return m_min_t;
}

const float &ParametrizedSurfaceGenerator::max_t() const {
    return m_max_t;
}

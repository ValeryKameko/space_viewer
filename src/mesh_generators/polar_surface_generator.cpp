#include "include/mesh_generators/polar_surface_generator.h"

#include <ast_nodes/ast_function_node.h>
#include <ast_nodes/ast_variable_node.h>

#include <parser.h>

#include <mesh_generators/parametrized_surface_generator.h>
#include <mesh_generators/uvsphere_generator.h>

#include <meshes/composite_mesh.h>
#include <meshes/polygon_mesh.h>
#include <meshes/quad_mesh.h>
#include <meshes/triangle_mesh.h>

#include <QQuaternion>
#include <QtMath>

PolarSurfaceGenerator::PolarSurfaceGenerator()
    : MeshGenerator()
    , m_function_r(nullptr) {

}

PolarSurfaceGenerator::~PolarSurfaceGenerator() {

}

QSharedPointer<Mesh> PolarSurfaceGenerator::generate() {
    if (!m_function_r)
        return nullptr;
    QSharedPointer<CompositeMesh> mesh(new CompositeMesh());
    for (unsigned int i = 0; i < m_longitude_subdivision_level; i++) {
        float left_longitude_angle = 2 * M_PI * i / (float)m_longitude_subdivision_level;
        float right_longitude_angle = 2 * M_PI * (i + 1) / (float)m_longitude_subdivision_level;

        QSharedPointer<Mesh> segment = generateSegment(left_longitude_angle,
                                                       right_longitude_angle);
        if (!segment)
            return nullptr;
        mesh->meshes().append(segment);
    }
    return mesh;
}

QSharedPointer<Mesh> PolarSurfaceGenerator::generateSegment(float left_longitude_angle, float right_longitude_angle) {
    QSharedPointer<CompositeMesh> mesh(new CompositeMesh());
    for (unsigned int i = 0; i < m_lattitude_subdivision_level; i++) {
        float left_lattitude_angle = M_PI * i / (float)m_lattitude_subdivision_level - M_PI_2;
        float right_lattitude_angle = M_PI * (i + 1) / (float)m_lattitude_subdivision_level - M_PI_2;

        QSharedPointer<PolygonMesh> polygon;
        QSharedPointer<QVector3D> vertex;
        if (i == 0) {
            QSharedPointer<TriangleMesh> triangle_polygon(new TriangleMesh());

            for (const QPair<float, float> &angles : QVector<QPair<float, float>>{
                {left_longitude_angle, left_lattitude_angle},
                {left_longitude_angle, right_lattitude_angle},
                {right_longitude_angle, right_lattitude_angle}})
            {

                vertex = generateVertex(angles.first, angles.second);
                if (!vertex)
                    return nullptr;
                triangle_polygon->vertices().append(*vertex);
            }

            polygon = triangle_polygon;
        } else if (i == m_lattitude_subdivision_level - 1) {
            QSharedPointer<TriangleMesh> triangle_polygon(new TriangleMesh());
            for (const QPair<float, float> &angles : QVector<QPair<float, float>>{
                {left_longitude_angle, left_lattitude_angle},
                {right_longitude_angle, left_lattitude_angle},
                {right_longitude_angle, right_lattitude_angle}})
            {

                vertex = generateVertex(angles.first, angles.second);
                if (!vertex)
                    return nullptr;
                triangle_polygon->vertices().append(*vertex);
            }

            polygon = triangle_polygon;
        } else {
            QSharedPointer<QuadMesh> quad_polygon(new QuadMesh());

            QSharedPointer<TriangleMesh> triangle_polygon(new TriangleMesh());
            for (const QPair<float, float> &angles : QVector<QPair<float, float>>{
                {left_longitude_angle, left_lattitude_angle},
                {left_longitude_angle, right_lattitude_angle},
                {right_longitude_angle, right_lattitude_angle},
                {right_longitude_angle, left_lattitude_angle}})
            {

                vertex = generateVertex(angles.first, angles.second);
                if (!vertex)
                    return nullptr;
                quad_polygon->vertices().append(*vertex);
            }

            polygon = quad_polygon;
        }
        mesh->meshes().append(polygon);

    }
    return mesh;
}

QSharedPointer<QVector3D> PolarSurfaceGenerator::generateVertex(float longitude_angle, float lattitude_angle) {
    std::swap(longitude_angle, lattitude_angle);
    float longitude_angle_degrees = qRadiansToDegrees(longitude_angle);
    float lattitude_angle_degrees = qRadiansToDegrees(lattitude_angle);
    QVector3D vertex(0, 0, 1);

    float multiplier = m_function_r->evaluate(QMap<QString, float>{
                                                {QString("phi"), longitude_angle + M_PI_2},
                                                {QString("theta"), lattitude_angle}
                                              });
    if (std::isnan(multiplier))
        return nullptr;

    vertex *= multiplier;
    vertex = QQuaternion::fromAxisAndAngle(QVector3D(0, 1, 0), longitude_angle_degrees).rotatedVector(vertex);
    vertex = QQuaternion::fromAxisAndAngle(QVector3D(1, 0, 0), lattitude_angle_degrees).rotatedVector(vertex);

    return QSharedPointer<QVector3D>(new QVector3D(vertex));
}

QSharedPointer<ASTNode> &PolarSurfaceGenerator::function_r() {
    return m_function_r;
}

const QSharedPointer<ASTNode> &PolarSurfaceGenerator::function_r() const {
    return m_function_r;
}

unsigned int &PolarSurfaceGenerator::lattitude_subdivision_level() {
    return m_lattitude_subdivision_level;
}

unsigned int &PolarSurfaceGenerator::longitude_subdivision_level() {
    return m_longitude_subdivision_level;
}

const unsigned int &PolarSurfaceGenerator::lattitude_subdivision_level() const {
    return m_lattitude_subdivision_level;
}

const unsigned int &PolarSurfaceGenerator::longitude_subdivision_level() const {
    return m_longitude_subdivision_level;
}

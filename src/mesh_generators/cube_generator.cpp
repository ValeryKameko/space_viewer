#include "mesh_generators/cube_generator.h"
#include "meshes/composite_mesh.h"
#include "meshes/quad_mesh.h"
#include <QVector3D>
#include <QVector>

CubeGenerator::CubeGenerator(float size)
    : MeshGenerator(), m_size(size) {

}

const float &CubeGenerator::size() const {
    return m_size;
}

float &CubeGenerator::size() {
    return m_size;
}

QSharedPointer<Mesh> CubeGenerator::generate() {
    QSharedPointer<CompositeMesh> mesh(new CompositeMesh());
    mesh->meshes().append(generateFace(QVector3D(-1, 0, 0)));
    mesh->meshes().append(generateFace(QVector3D(+1, 0, 0)));

    mesh->meshes().append(generateFace(QVector3D(0, -1, 0)));
    mesh->meshes().append(generateFace(QVector3D(0, +1, 0)));

    mesh->meshes().append(generateFace(QVector3D(0, 0, -1)));
    mesh->meshes().append(generateFace(QVector3D(0, 0, +1)));

    return mesh;
}

QSharedPointer<Mesh> CubeGenerator::generateFace(const QVector3D &normal) {
    QSharedPointer<QuadMesh> mesh(new QuadMesh());

    if (std::fabs(normal.x()) > 1e-5) {
        mesh->vertices().append(QVector3D(normal.x(), -1, -1));
        mesh->vertices().append(QVector3D(normal.x(), -1, +1));
        mesh->vertices().append(QVector3D(normal.x(), +1, +1));
        mesh->vertices().append(QVector3D(normal.x(), +1, -1));
    }

    if (std::fabs(normal.y()) > 1e-5) {
        mesh->vertices().append(QVector3D(-1, normal.y(), -1));
        mesh->vertices().append(QVector3D(-1, normal.y(), +1));
        mesh->vertices().append(QVector3D(+1, normal.y(), +1));
        mesh->vertices().append(QVector3D(+1, normal.y(), -1));
    }

    if (std::fabs(normal.z()) > 1e-5) {
        mesh->vertices().append(QVector3D(-1, -1, normal.z()));
        mesh->vertices().append(QVector3D(-1, +1, normal.z()));
        mesh->vertices().append(QVector3D(+1, +1, normal.z()));
        mesh->vertices().append(QVector3D(+1, -1, normal.z()));
    }

    for (QVector3D &vertex : mesh->vertices())
        vertex *= m_size;
    return mesh;
}

#include "mesh_generators/tetrahedron_generator.h"
#include "meshes/composite_mesh.h"
#include "meshes/triangle_mesh.h"
#include <algorithm>

TetrahedronGenerator::TetrahedronGenerator(float radius)
    : m_radius(radius) {

}

TetrahedronGenerator::~TetrahedronGenerator() {

}

QSharedPointer<Mesh> TetrahedronGenerator::generate() {
    QSharedPointer<CompositeMesh> mesh(new CompositeMesh());

    QVector3D vertices[] = {
        m_radius * QVector3D(+std::sqrt(8.0 / 9.0), 0.0, - 1.0 / 3.0),
        m_radius * QVector3D(-std::sqrt(2.0 / 9.0), +std::sqrt(2.0 / 3.0), - 1.0 / 3.0),
        m_radius * QVector3D(-std::sqrt(2.0 / 9.0), -std::sqrt(2.0 / 3.0), - 1.0 / 3.0),
        m_radius * QVector3D(0.0, 0.0, 1.0)
    };

    QVector<QVector<unsigned int> > faces_ids {
        QVector<unsigned int>{3, 0, 1},
        QVector<unsigned int>{3, 1, 2},
        QVector<unsigned int>{3, 2, 0},
        QVector<unsigned int>{0, 1, 2}
    };

    for (const QVector<unsigned int> &face_ids : faces_ids) {
        QSharedPointer<TriangleMesh> face(new TriangleMesh());
        for (const unsigned int &id : face_ids)
            face->vertices().append(vertices[id]);

        mesh->meshes().append(face);
    }

    return mesh;
}

const float &TetrahedronGenerator::radius() const {
    return m_radius;
}

float &TetrahedronGenerator::radius() {
    return m_radius;
}

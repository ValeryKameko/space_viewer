#include "mesh_generators/torus_generator.h"
#include "meshes/composite_mesh.h"
#include "meshes/quad_mesh.h"
#include <QTransform>
#include <QQuaternion>
#include <QMatrix4x4>
#include <QtMath>

TorusGenerator::TorusGenerator(float major_radius, float minor_radius)
    : m_major_radius(major_radius)
    , m_minor_radius(minor_radius)
    , m_major_subdivision(10)
    , m_minor_subdivision(6) {

}


QSharedPointer<Mesh> TorusGenerator::generate() {
    QSharedPointer<CompositeMesh> mesh(new CompositeMesh());

    for (unsigned int i = 0; i < m_major_subdivision; i++) {
        float left_major_angle = 2 * M_PI * i / (float)m_major_subdivision;
        float right_major_angle = 2 * M_PI * (i + 1) / (float)m_major_subdivision;
        mesh->meshes().append(generateRing(left_major_angle, right_major_angle));
    }

    return mesh;
}

QSharedPointer<Mesh> TorusGenerator::generateRing(float left_major_angle, float right_major_angle) {
    QSharedPointer<CompositeMesh> mesh(new CompositeMesh());

    for (unsigned int i = 0; i < m_minor_subdivision; i++) {
        float left_minor_angle = 2 * M_PI * i / (float)m_minor_subdivision;
        float right_minor_angle = 2 * M_PI * (i + 1) / (float)m_minor_subdivision;
        mesh->meshes().append(generateFace(left_major_angle, right_major_angle, left_minor_angle, right_minor_angle));
    }

    return mesh;
}

QSharedPointer<Mesh> TorusGenerator::generateFace(float left_major_angle, float right_major_angle, float left_minor_angle, float right_minor_angle) {
    QSharedPointer<QuadMesh> mesh(new QuadMesh());

    for (const float &major_angle : {left_major_angle, right_major_angle}) {
        for (const float &minor_angle : {left_minor_angle, right_minor_angle}) {
            float major_angle_degrees = qRadiansToDegrees(major_angle);
            float minor_angle_degrees = qRadiansToDegrees(minor_angle);

            QVector3D vertex(0, 0, 0);
            vertex += m_minor_radius * QVector3D(0, 0, 1);
            vertex = QQuaternion::fromAxisAndAngle(QVector3D(1, 0, 0), minor_angle_degrees).rotatedVector(vertex);
            vertex += m_major_radius * QVector3D(0, 0, 1);
            vertex = QQuaternion::fromAxisAndAngle(QVector3D(0, 1, 0), major_angle_degrees).rotatedVector(vertex);

            mesh->vertices().append(vertex);
        }
    }
    std::swap(mesh->vertices()[2], mesh->vertices()[3]);

    return mesh;
}

const float &TorusGenerator::major_radius() const {
    return m_major_radius;
}

float &TorusGenerator::major_radius() {
    return m_major_radius;
}

const float &TorusGenerator::minor_radius() const {
    return m_minor_radius;
}

float &TorusGenerator::minor_radius() {
    return m_minor_radius;
}

const unsigned int &TorusGenerator::major_subdivision() const {
    return m_major_subdivision;
}

unsigned int &TorusGenerator::major_subdivision() {
    return m_major_subdivision;
}

const unsigned int &TorusGenerator::minor_subdivision() const {
    return m_minor_subdivision;
}

unsigned int &TorusGenerator::minor_subdivision() {
    return m_minor_subdivision;
}

#include "mesh_generators/uvsphere_generator.h"
#include "meshes/composite_mesh.h"
#include "meshes/triangle_mesh.h"
#include "meshes/quad_mesh.h"
#include <QQuaternion>
#include <QtMath>

UVSphereGenerator::UVSphereGenerator(float radius)
    : m_radius(radius)
    , m_longitude_subdivision(20)
    , m_lattitude_subdivision(10) {

}

QSharedPointer<Mesh> UVSphereGenerator::generate() {
    QSharedPointer<CompositeMesh> mesh(new CompositeMesh());
    for (unsigned int i = 0; i < m_longitude_subdivision; i++) {
        float left_longitude_angle = 2 * M_PI * i / (float)m_longitude_subdivision;
        float right_longitude_angle = 2 * M_PI * (i + 1) / (float)m_longitude_subdivision;

        mesh->meshes().append(generateSegment(left_longitude_angle,
                                              right_longitude_angle));
    }
    return mesh;
}

QSharedPointer<Mesh> UVSphereGenerator::generateSegment(float left_longitude_angle, float right_longitude_angle) {
    QSharedPointer<CompositeMesh> mesh(new CompositeMesh());
    for (unsigned int i = 0; i < m_lattitude_subdivision; i++) {
        float left_lattitude_angle = M_PI * i / (float)m_lattitude_subdivision - M_PI_2;
        float right_lattitude_angle = M_PI * (i + 1) / (float)m_lattitude_subdivision - M_PI_2;

        if (i == 0) {
            QSharedPointer<TriangleMesh> polygon(new TriangleMesh());
            polygon->vertices().append(generateVertex(left_longitude_angle, left_lattitude_angle));
            polygon->vertices().append(generateVertex(left_longitude_angle, right_lattitude_angle));
            polygon->vertices().append(generateVertex(right_longitude_angle, right_lattitude_angle));

            mesh->meshes().append(polygon);
        } else if (i == m_lattitude_subdivision - 1) {
            QSharedPointer<TriangleMesh> polygon(new TriangleMesh());
            polygon->vertices().append(generateVertex(left_longitude_angle, left_lattitude_angle));
            polygon->vertices().append(generateVertex(right_longitude_angle, left_lattitude_angle));
            polygon->vertices().append(generateVertex(right_longitude_angle, right_lattitude_angle));

            mesh->meshes().append(polygon);
        } else {
            QSharedPointer<QuadMesh> polygon(new QuadMesh());
            polygon->vertices().append(generateVertex(left_longitude_angle, left_lattitude_angle));
            polygon->vertices().append(generateVertex(left_longitude_angle, right_lattitude_angle));
            polygon->vertices().append(generateVertex(right_longitude_angle, right_lattitude_angle));
            polygon->vertices().append(generateVertex(right_longitude_angle, left_lattitude_angle));

            mesh->meshes().append(polygon);
        }
    }
    return mesh;
}

QVector3D UVSphereGenerator::generateVertex(float longitude_angle, float lattitude_angle) {
    std::swap(longitude_angle, lattitude_angle);
    float longitude_angle_degrees = qRadiansToDegrees(longitude_angle);
    float lattitude_angle_degrees = qRadiansToDegrees(lattitude_angle);
    QVector3D vertex(0, 0, 1);
    vertex *= m_radius;
    vertex = QQuaternion::fromAxisAndAngle(QVector3D(0, 1, 0), longitude_angle_degrees).rotatedVector(vertex);
    vertex = QQuaternion::fromAxisAndAngle(QVector3D(1, 0, 0), lattitude_angle_degrees).rotatedVector(vertex);
    return vertex;
}

const float &UVSphereGenerator::radius() const {
    return m_radius;
}

float &UVSphereGenerator::radius() {
    return m_radius;
}

const unsigned int &UVSphereGenerator::longitude_subdivision() const {
    return m_longitude_subdivision;
}

unsigned int &UVSphereGenerator::longitude_subdivision() {
    return m_longitude_subdivision;
}

const unsigned int &UVSphereGenerator::lattitude_subdivision() const {
    return m_lattitude_subdivision;
}

unsigned int &UVSphereGenerator::lattitude_subdivision() {
    return m_lattitude_subdivision;
}

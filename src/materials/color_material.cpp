#include "materials/color_material.h"

ColorMaterial::ColorMaterial(const QColor &color)
    : m_color(color) {

}

ColorMaterial::~ColorMaterial() {

}

const QColor &ColorMaterial::color() const {
    return m_color;
}

QColor &ColorMaterial::color() {
    return m_color;
}

#include "include/gui/decartfunctiondialog.h"
#include "ui_decartfunctiondialog.h"

#include <QDoubleValidator>
#include <QMessageBox>
#include <mesh.h>
#include <parser.h>

#include <mesh_generators/z_axis_function_generator.h>

DecartFunctionDialog::DecartFunctionDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DecartFunctionDialog)
{
    ui->setupUi(this);
}

DecartFunctionDialog::~DecartFunctionDialog()
{
    delete ui;
}

QSharedPointer<Mesh> DecartFunctionDialog::generatedMesh() {
    return m_mesh;
}

void DecartFunctionDialog::on_generateButton_clicked() {
    QString min_x_string = ui->fromXEdit->text();
    QString max_x_string = ui->toXEdit->text();
    QString min_y_string = ui->fromYEdit->text();
    QString max_y_string = ui->toYEdit->text();

    QString subdivision_x_string = ui->subdivisionXEdit->text();
    QString subdivision_y_string = ui->subdivisionYEdit->text();

    QString function_string = ui->functionEdit->text();

    QDoubleValidator min_x_validator(-100, 100, 5);
    QDoubleValidator max_x_validator(-100, 100, 5);
    QDoubleValidator min_y_validator(-100, 100, 5);
    QDoubleValidator max_y_validator(-100, 100, 5);

    QIntValidator subdivision_x_validator(1, 100);
    QIntValidator subdivision_y_validator(1, 100);

    int pos = 0;

    if (min_x_validator.validate(min_x_string, pos) != QValidator::State::Acceptable) {
        QMessageBox::critical(this, "Ошибка",
                              "Поле минимального значения промежутка X содержит некорректные данные.");
        return;
    }
    if (max_x_validator.validate(max_x_string, pos) != QValidator::State::Acceptable) {
        QMessageBox::critical(this, "Ошибка",
                              "Поле максимального значения промежутка X содержит некорректные данные.");
        return;
    }

    if (min_y_validator.validate(min_y_string, pos) != QValidator::State::Acceptable) {
        QMessageBox::critical(this, "Ошибка",
                              "Поле минимального значения промежутка Y содержит некорректные данные.");
        return;
    }

    if (max_y_validator.validate(max_y_string, pos) != QValidator::State::Acceptable) {
        QMessageBox::critical(this, "Ошибка",
                              "Поле максимального значения промежутка Y содержит некорректные данные.");
        return;
    }

    if (subdivision_x_validator.validate(subdivision_x_string, pos) != QValidator::State::Acceptable) {
        QMessageBox::critical(this, "Ошибка",
                              "Поле детализации по оси X содержит некорректные данные.");
        return;
    }

    if (subdivision_y_validator.validate(subdivision_y_string, pos) != QValidator::State::Acceptable) {
        QMessageBox::critical(this, "Ошибка",
                              "Поле детализации по оси Y содержит некорректные данные.");
        return;
    }

    float min_x = min_x_string.toFloat();
    float max_x = max_y_string.toFloat();
    float min_y = min_x_string.toFloat();
    float max_y = max_y_string.toFloat();

    unsigned int subdivision_x = subdivision_x_string.toUInt();
    unsigned int subdivision_y = subdivision_y_string.toUInt();

    if (min_x > max_x) {
        QMessageBox::critical(this, "Ошибка",
                              "Минимальное значение промежутка X больше максимального значения промежутка X.");
        return;
    }
    if (min_y > max_y) {
        QMessageBox::critical(this, "Ошибка",
                              "Минимальное значение промежутка Y больше максимального значения промежутка Y.");
        return;
    }

    m_min_x = min_x;
    m_max_x = max_x;
    m_min_y = min_y;
    m_max_y = max_y;

    m_subdivision_x = subdivision_x;
    m_subdivision_y = subdivision_y;

    Parser parser;

    QSharedPointer<ASTNode> function = parser.parse(function_string);

    if (!function) {
        QMessageBox::critical(this, "Ошибка",
                              "Поле функция сожержит некорректные данные.");
        return;
    }
    m_function = function;

    ZAxisFunctionGenerator generator;
    generator.min_x() = m_min_x;
    generator.max_x() = m_max_x;
    generator.min_y() = m_min_y;
    generator.max_y() = m_max_y;

    generator.x_subdivision_level() = m_subdivision_x;
    generator.y_subdivision_level() = m_subdivision_y;

    generator.function() = m_function;

    m_mesh = generator.generate();

    if (!m_mesh) {
        QMessageBox::critical(this, "Ошибка",
                              "Поле функция сожержит некорректные данные.");
        return;
    }

    accept();
    return;
}

void DecartFunctionDialog::on_cancelButton_clicked() {
    reject();
}

#include "include/gui/spheredialog.h"
#include "ui_spheredialog.h"

#include <QDoubleValidator>
#include <QIntValidator>
#include <QMessageBox>

#include <mesh_generators/uvsphere_generator.h>

SphereDialog::SphereDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SphereDialog)
{
    ui->setupUi(this);
}

SphereDialog::~SphereDialog()
{
    delete ui;
}

QSharedPointer<Mesh> SphereDialog::generatedMesh() {
    QSharedPointer<Mesh> mesh(nullptr);

    UVSphereGenerator generator;
    generator.radius() = m_radius;
    generator.lattitude_subdivision() = m_latitude_subdivide_level;
    generator.longitude_subdivision() = m_longitude_subdivide_level;

    mesh = generator.generate();

    return mesh;
}

void SphereDialog::on_generateButton_clicked() {
    QDoubleValidator radius_validator(1.0, 100.0, 5);
    QIntValidator latitude_subdivide_level_validator(2, 50);
    QIntValidator longitude_subdivide_level_validator(2, 50);

    int pos = 0;

    QString radius_string = ui->radiusEdit->text();
    QString latitude_subdivide_level_string = ui->latitudeEdit->text();
    QString longitude_subdivide_level_string = ui->longitudeEdit->text();

    if (radius_validator.validate(radius_string, pos) != QValidator::State::Acceptable) {
        QMessageBox::critical(this, "Ошибка", "Поле радиус с одержит некорректные данные");
        return;
    }
    if (latitude_subdivide_level_validator.validate(latitude_subdivide_level_string, pos) != QValidator::State::Acceptable) {
        QMessageBox::critical(this, "Ошибка",
                              "Поле детализации по широте с одержит некорректные данные");
        return;
    }
    if (longitude_subdivide_level_validator.validate(longitude_subdivide_level_string, pos) != QValidator::State::Acceptable) {
        QMessageBox::critical(this, "Ошибка",
                              "Поле детализации по долготе с одержит некорректные данные");
        return;
    }

    float radius = radius_string.toFloat();
    unsigned int latitude_subdivide_level = latitude_subdivide_level_string.toUInt();
    unsigned int longitude_subdivide_level = longitude_subdivide_level_string.toUInt();

    m_radius = radius;
    m_latitude_subdivide_level = latitude_subdivide_level;
    m_longitude_subdivide_level = longitude_subdivide_level;

    this->accept();
}

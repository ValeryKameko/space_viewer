#include "include/gui/parametrizedsurfacedialog.h"
#include "ui_parametrizedsurfacedialog.h"

#include <QValidator>
#include <QMessageBox>
#include <parser.h>
#include <mesh_generators/parametrized_surface_generator.h>

ParametrizedSurfaceDialog::ParametrizedSurfaceDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ParametrizedSurfaceDialog)
{
    ui->setupUi(this);
}

ParametrizedSurfaceDialog::~ParametrizedSurfaceDialog()
{
    delete ui;
}

QSharedPointer<Mesh> ParametrizedSurfaceDialog::generatedMesh() {
    return m_mesh;
}

void ParametrizedSurfaceDialog::on_generate_button_clicked() {
    QString min_s_string = ui->fromSEdit->text();
    QString max_s_string = ui->toSEdit->text();
    QString min_t_string = ui->fromTEdit->text();
    QString max_t_string = ui->toTEdit->text();

    QString subdivision_s_string = ui->subdivisionSEdit->text();
    QString subdivision_t_string = ui->subdivisionTEdit->text();

    QString function_x_string = ui->functionXEdit->text();
    QString function_y_string = ui->functionYEdit->text();
    QString function_z_string = ui->functionZEdit->text();

    QDoubleValidator min_s_validator(-100, 100, 5);
    QDoubleValidator max_s_validator(-100, 100, 5);
    QDoubleValidator min_t_validator(-100, 100, 5);
    QDoubleValidator max_t_validator(-100, 100, 5);

    QIntValidator subdivision_s_validator(1, 100);
    QIntValidator subdivision_t_validator(1, 100);

    int pos = 0;

    if (min_s_validator.validate(min_s_string, pos) != QValidator::State::Acceptable) {
        QMessageBox::critical(this, "Ошибка",
                              "Поле минимального значения промежутка S содержит некорректные данные.");
        return;
    }
    if (max_s_validator.validate(max_s_string, pos) != QValidator::State::Acceptable) {
        QMessageBox::critical(this, "Ошибка",
                              "Поле максимального значения промежутка S содержит некорректные данные.");
        return;
    }

    if (min_t_validator.validate(min_t_string, pos) != QValidator::State::Acceptable) {
        QMessageBox::critical(this, "Ошибка",
                              "Поле минимального значения промежутка T содержит некорректные данные.");
        return;
    }

    if (max_t_validator.validate(max_t_string, pos) != QValidator::State::Acceptable) {
        QMessageBox::critical(this, "Ошибка",
                              "Поле максимального значения промежутка T содержит некорректные данные.");
        return;
    }

    if (subdivision_s_validator.validate(subdivision_s_string, pos) != QValidator::State::Acceptable) {
        QMessageBox::critical(this, "Ошибка",
                              "Поле детализации параметра S содержит некорректные данные.");
        return;
    }

    if (subdivision_t_validator.validate(subdivision_t_string, pos) != QValidator::State::Acceptable) {
        QMessageBox::critical(this, "Ошибка",
                              "Поле детализации параметра T содержит некорректные данные.");
        return;
    }

    float min_s = min_s_string.toFloat();
    float max_s = max_s_string.toFloat();
    float min_t = min_t_string.toFloat();
    float max_t = max_t_string.toFloat();

    unsigned int subdivision_s = subdivision_s_string.toUInt();
    unsigned int subdivision_t = subdivision_t_string.toUInt();

    if (min_s > max_s) {
        QMessageBox::critical(this, "Ошибка",
                              "Минимальное значение промежутка S больше максимального значения промежутка S.");
        return;
    }
    if (min_t > max_t) {
        QMessageBox::critical(this, "Ошибка",
                              "Минимальное значение промежутка T больше максимального значения промежутка T.");
        return;
    }

    m_min_s = min_s;
    m_max_s = max_s;
    m_min_t = min_t;
    m_max_t = max_t;

    m_subdivision_s = subdivision_s;
    m_subdivision_t = subdivision_t;

    Parser parser;

    QSharedPointer<ASTNode> function_x = parser.parse(function_x_string);
    QSharedPointer<ASTNode> function_y = parser.parse(function_y_string);
    QSharedPointer<ASTNode> function_z = parser.parse(function_z_string);


    if (!function_x || !function_y || !function_z) {
        QMessageBox::critical(this, "Ошибка",
                              "Поле функций содержит некорректные данные.");
        return;
    }
    m_function_x = function_x;
    m_function_y = function_y;
    m_function_z = function_z;

    ParametrizedSurfaceGenerator generator;
    generator.min_s() = m_min_s;
    generator.max_s() = m_max_s;
    generator.min_t() = m_min_t;
    generator.max_t() = m_max_t;

    generator.s_subdivision_level() = m_subdivision_s;
    generator.t_subdivision_level() = m_subdivision_t;

    generator.function_x() = m_function_x;
    generator.function_y() = m_function_y;
    generator.function_z() = m_function_z;

    m_mesh = generator.generate();

    if (!m_mesh) {
        QMessageBox::critical(this, "Ошибка",
                              "Заданные функции некорректны.");
        return;
    }

    accept();
    return;
}

void ParametrizedSurfaceDialog::on_cancel_button_clicked() {
    reject();
}

#include "include/gui/cubedialog.h"
#include "ui_cubedialog.h"

#include <QDoubleValidator>
#include <QMessageBox>

#include <mesh_generators/cube_generator.h>

#include <mesh_transformers/quad_subdivider_transformer.h>

CubeDialog::CubeDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CubeDialog)
{
    ui->setupUi(this);
}

CubeDialog::~CubeDialog()
{
    delete ui;
}

QSharedPointer<Mesh> CubeDialog::generatedMesh() {
    QSharedPointer<Mesh> mesh;
    CubeGenerator generator;
    generator.size() = m_size;

    mesh = generator.generate();

    QuadSubdividerTransformer transformer;
    transformer.subdivision_count() = m_subdivision;

    mesh = transformer.transform(mesh);
    return mesh;
}

void CubeDialog::on_generateButton_clicked() {
    QIntValidator subdivision_validator(1, 50);
    QDoubleValidator size_validator(1.0, 100.0, 5);

    QString subdivision_string = ui->subdivisionEdit->text();
    QString size_string = ui->sizeEdit->text();

    int pos;

    if (size_validator.validate(size_string, pos) != QValidator::State::Acceptable) {
        QMessageBox::critical(this, "Ошибка",
                              "Поле размер содержит некорректные данные");
        return;
    }

    if (subdivision_validator.validate(subdivision_string, pos) != QValidator::State::Acceptable) {
        QMessageBox::critical(this, "Ошибка",
                              "Поле детализации содержит некорректные данные");
        return;
    }

    m_size = size_string.toFloat();
    m_subdivision = subdivision_string.toUInt();

    accept();
    return;
}

void CubeDialog::on_cancelButton_clicked() {
    reject();
}

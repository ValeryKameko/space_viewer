#include "include\gui\tetrahedrondialog.h"
#include "ui_tetrahedrondialog.h"
#include <QDoubleValidator>
#include <QIntValidator>
#include <QMessageBox>
#include <mesh_transformers/triangle_subdivider_transformer.h>

TetrahedronDialog::TetrahedronDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TetrahedronDialog)
{
    ui->setupUi(this);
}

TetrahedronDialog::~TetrahedronDialog()
{
    delete ui;
}

QSharedPointer<Mesh> TetrahedronDialog::generatedMesh() {
    QSharedPointer<Mesh> mesh(nullptr);
    TetrahedronGenerator generator(m_size);
    TriangleSubdividerTransformer transformer(m_subdivision_level);
    mesh = generator.generate();
    mesh = transformer.transform(mesh);
    return mesh;
}

void TetrahedronDialog::on_generateButton_clicked() {
    QDoubleValidator size_validator(1.0, 100.0, 5);
    QIntValidator subdivision_level_validator(1, 20);

    int pos;
    QString size_string = ui->sizeEdit->text();
    QString subdivision_level_string = ui->subdivisionLevelEdit->text();

    if (size_validator.validate(size_string, pos) != QValidator::State::Acceptable) {
        QMessageBox::critical(this, "Ошибка", "Поле размер содержит некорректные данные");
        return;
    }

    if (subdivision_level_validator.validate(subdivision_level_string, pos) != QValidator::State::Acceptable) {
        QMessageBox::critical(this, "Ошибка", "Поле детализация содержит некорректные данные");
        return;
    }

    float size = size_string.toFloat();
    int subdivision_level = subdivision_level_string.toInt();

    m_size = size;
    m_subdivision_level = subdivision_level;
    this->accept();
    return;
}

void TetrahedronDialog::on_cancelButton_clicked() {
    this->reject();
}

#include "include/gui/torusdialog.h"
#include "ui_torusdialog.h"

#include <QDoubleValidator>
#include <QMessageBox>

#include <mesh_generators/torus_generator.h>

TorusDialog::TorusDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TorusDialog)
{
    ui->setupUi(this);
}

TorusDialog::~TorusDialog()
{
    delete ui;
}

QSharedPointer<Mesh> TorusDialog::generatedMesh() {
    return m_mesh;
}

void TorusDialog::on_generateButton_clicked() {
    QDoubleValidator big_radius_validator(1, 100, 5);
    QDoubleValidator little_radius_validator(1, 100, 5);

    QIntValidator big_radius_subdivision_validator(1, 100);
    QIntValidator little_radius_subdivision_validator(1, 100);

    int pos = 0;

    QString big_radius_string = ui->bigRadiusEdit->text();
    QString little_radius_string = ui->littleRadiusEdit->text();
    QString big_radius_subdivision_string = ui->subdivisionBigRadiusLevelEdit->text();
    QString little_radius_subdivision_string = ui->subdivisionLittleLevelEdit->text();

    if (big_radius_validator.validate(big_radius_string, pos) != QValidator::State::Acceptable) {
        QMessageBox::critical(this, "Ошибка",
                              "Поле большого радиуса содержит некорректные данные.");
        return;
    }
    if (little_radius_validator.validate(little_radius_string, pos) != QValidator::State::Acceptable) {
        QMessageBox::critical(this, "Ошибка",
                              "Поле малого радиуса содержит некорректные данные.");
        return;
    }

    if (big_radius_subdivision_validator.validate(big_radius_subdivision_string, pos) != QValidator::State::Acceptable) {
        QMessageBox::critical(this, "Ошибка",
                              "Поле детализации по большему радиусу содержит некорректные данные.");
        return;
    }

    if (little_radius_subdivision_validator.validate(little_radius_subdivision_string, pos) != QValidator::State::Acceptable) {
        QMessageBox::critical(this, "Ошибка",
                              "Поле детализации по малому радиусу содержит некорректные данные.");
        return;
    }

    float big_radius = big_radius_string.toFloat();
    float little_radius = little_radius_string.toFloat();
    unsigned int big_radius_subdivision= big_radius_subdivision_string.toUInt();
    unsigned int little_radius_subdivision= little_radius_subdivision_string.toUInt();

    if (big_radius < little_radius) {
        QMessageBox::critical(this, "Ошибка",
                              "Малый радиус должен быть меньше большого.");
        return;
    }

    TorusGenerator generator;
    generator.minor_radius() = little_radius;
    generator.major_radius() = big_radius;
    generator.minor_subdivision() = little_radius_subdivision;
    generator.major_subdivision() = big_radius_subdivision;

    m_mesh = generator.generate();
    accept();
}

void TorusDialog::on_cancelButton_clicked() {
    reject();
}

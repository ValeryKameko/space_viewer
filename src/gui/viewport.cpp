#include "include/gui/viewport.h"
#include "mesh_transformers/quad_subdivider_transformer.h"
#include "mesh_transformers/spherify_transformer.h"
#include "mesh_transformers/transformation_transformer.h"
#include "mesh_transformers/triangle_subdivider_transformer.h"
#include "meshes/line_mesh.h"
#include "mesh_generators/cube_generator.h"
#include "mesh_generators/torus_generator.h"
#include "mesh_generators/uvsphere_generator.h"
#include "mesh_generators/tetrahedron_generator.h"
#include "mesh_generators/pyramid_generator.h"
#include "mesh_transformers/transformation_transformer.h"
#include "materials/color_material.h"
#include <QWheelEvent>
#include <QtMath>
#include <QInputEvent>

Viewport::Viewport(QWidget * parent)
    : QOpenGLWidget(parent)
    , m_wireframe_model(new Model())
    , m_wireframe_renderer(new WireframeRenderer())
    , m_point_model(new Model())
    , m_point_renderer(new PointRenderer())
    , m_camera(new Camera())
    , m_polygon_renderer(new PolygonRenderer())
    , m_polygon_model(new Model())
    , m_camera_distance(50.0)
    , m_camera_rotation()
    , m_gesture_type(GestureType::None)
    , m_camera_center()
        {

    QSurfaceFormat surface_format;
    surface_format.setSamples(16);
    this->setFormat(surface_format);

    m_wireframe_renderer->camera() = m_camera;
    m_point_renderer->camera() = m_camera;
    m_point_renderer.dynamicCast<PointRenderer>()->point_size() = 5.0;
    m_polygon_renderer->camera() = m_camera;

    setMatrices();
}

Viewport::~Viewport() {
}

void Viewport::initializeGL() {
    initializeOpenGLFunctions();

    connect(context(), &QOpenGLContext::aboutToBeDestroyed,
            (Viewport*)this, &Viewport::uninitialize);

    for (const QSharedPointer<Renderer> &renderer : {
         m_polygon_renderer,
         m_wireframe_renderer,
         m_point_renderer}) {
        renderer->setContext(context());
        renderer->initialize();

        QMatrix4x4 &model_matrix = renderer->model_matrix();
        model_matrix.setToIdentity();
    }
}

void Viewport::resizeGL(int w, int h) {
    glViewport(0, 0, w, h);
    QMatrix4x4 &projection_matrix = m_camera->projection_matrix();

    projection_matrix.setToIdentity();
    projection_matrix.perspective(60, w / (h + 1.0), 0.01f, 1000.0f);
//    projection_matrix.ortho(-100, 100, -100, 100, -1, 1);
}

void Viewport::paintGL() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(0.0, 0.0, 0.0, 1.0);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_MULTISAMPLE);

    if (m_viewEdges) {
        m_wireframe_renderer->model() = m_wireframe_model;
        m_wireframe_renderer->render();
    }
    if (m_viewPoints) {
        m_point_renderer->model() = m_point_model;
        m_point_renderer->render();
    }
    if (m_viewPolygons) {
        m_polygon_renderer->model() = m_polygon_model;
        m_polygon_renderer->render();
    }
}

void Viewport::uninitialize() {
    makeCurrent();

//    m_renderer->uninitialize();

    doneCurrent();
}

void Viewport::wheelEvent(QWheelEvent * event) {
    m_camera_distance += event->angleDelta().y() / 100.0f;
    m_camera_distance = std::max(m_camera_distance, 0.0f);
    m_camera_distance = std::min(m_camera_distance, 500.0f);
    setMatrices();
    update();
}

void Viewport::mousePressEvent(QMouseEvent * event) {
    m_last_position = event->pos();
    if (event->modifiers().testFlag(Qt::KeyboardModifier::ControlModifier))
        m_gesture_type = GestureType::Moving;
    else
        m_gesture_type = GestureType::Rotation;
}

void Viewport::setMesh(const QSharedPointer<Mesh> &mesh) {
    m_polygon_model->mesh() = mesh;
    m_point_model->mesh() = mesh;
    m_wireframe_model->mesh() = mesh;

    update();
}

void Viewport::setWireframeMaterial(const QSharedPointer<Material> &material) {
    m_wireframe_model->material() = material;
    update();
}

void Viewport::setPointMaterial(const QSharedPointer<Material> &material) {
    m_point_model->material() = material;
    update();
}

void Viewport::setPolygonMaterial(const QSharedPointer<Material> &material) {
    m_polygon_model->material() = material;
    update();
}

void Viewport::setViewEdges(bool state) {
    m_viewEdges = state;
    update();
}

void Viewport::setViewPolygons(bool state) {
    m_viewPolygons = state;
    update();
}

void Viewport::setViewPoints(bool state) {
    m_viewPoints = state;
    update();
}

void Viewport::mouseMoveEvent(QMouseEvent * event) {
    if (m_gesture_type == GestureType::Rotation) {
        float camera_x_rotation, camera_y_rotation;
        camera_x_rotation = -(event->pos() - m_last_position).y() / 5.0f;
        camera_y_rotation = -(event->pos() - m_last_position).x() / 5.0f;

        m_camera_rotation *= QQuaternion::fromEulerAngles(
                    camera_x_rotation,
                    camera_y_rotation,
                    0.0f);

    } else {
        QVector3D camera_delta;
        camera_delta.setX(+(event->pos() - m_last_position).x() / 50.0f);
        camera_delta.setY(-(event->pos() - m_last_position).y() / 50.0f);

        camera_delta *= m_camera_distance / 10.0f;
        camera_delta = m_camera_rotation.rotatedVector(camera_delta);
//        camera_delta = m_camera->transformation_matrix().inverted().mapVector();

        m_camera_center -= camera_delta;
    }

    m_last_position = event->pos();
    setMatrices();
    update();
}

void Viewport::setMatrices() {
    QMatrix4x4 &transformation_matrix = m_camera->transformation_matrix();

    transformation_matrix.setToIdentity();

    QVector3D camera_forward(0.0, 0.0, 1.0);
    transformation_matrix.translate(m_camera_center);
    transformation_matrix.rotate(m_camera_rotation);
    transformation_matrix.translate(m_camera_distance * camera_forward);
}

#include "include\gui\mainwindow.h"
#include "model_loader.h"
#include "materials/color_material.h"
#include <QFileDialog>
#include <QMessageBox>
#include "ui_mainwindow.h"
#include <gui/tetrahedrondialog.h>
#include <gui/spheredialog.h>
#include <gui/ellipsoiddialog.h>
#include <gui/cubedialog.h>
#include <gui/decartfunctiondialog.h>
#include <gui/parametrizedsurfacedialog.h>
#include <gui/polarsurfacedialog.h>
#include <gui/torusdialog.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QObject::connect(ui->openFileAction, SIGNAL(triggered()),
                     this, SLOT(onOpenModelTriggered()));

    QObject::connect(ui->generateTetrahedronAction, SIGNAL(triggered()),
                     this, SLOT(onGenerateTetrahedronTriggered()));
    QObject::connect(ui->generateSphereAction, SIGNAL(triggered()),
                     this, SLOT(onGenerateSphereTriggered()));
    QObject::connect(ui->generateEllipsoidAction, SIGNAL(triggered()),
                     this, SLOT(onGenerateEllipsoidTriggered()));
    QObject::connect(ui->generateCubeAction, SIGNAL(triggered()),
                     this, SLOT(onGenerateCubeTriggered()));
    QObject::connect(ui->generateDecartFunctionAction, SIGNAL(triggered()),
                     this, SLOT(onGenerateDecartFunctionTriggered()));
    QObject::connect(ui->generateParametrizedSurfaceAction, SIGNAL(triggered()),
                     this, SLOT(onGenerateParametrizedSurfaceTriggered()));
    QObject::connect(ui->generatePolarFunctionAction, SIGNAL(triggered()),
                     this, SLOT(onGeneratePolarSurfaceTriggered()));
    QObject::connect(ui->generateTorusAction, SIGNAL(triggered()),
                     this, SLOT(onGenerateTorusTriggered()));

    QObject::connect(ui->viewEdgesAction, SIGNAL(toggled(bool)),
                     this, SLOT(onEdgesToggled(bool)));
    QObject::connect(ui->viewFacesAction, SIGNAL(toggled(bool)),
                     this, SLOT(onPolygonsToggled(bool)));
    QObject::connect(ui->viewPointsAction, SIGNAL(toggled(bool)),
                     this, SLOT(onPointsToggled(bool)));

    ui->viewport->setPointMaterial(QSharedPointer<Material>(new ColorMaterial(Qt::GlobalColor::white)));
    ui->viewport->setWireframeMaterial(QSharedPointer<Material>(new ColorMaterial(Qt::GlobalColor::blue)));
    ui->viewport->setPolygonMaterial(QSharedPointer<Material>(new ColorMaterial(Qt::GlobalColor::gray)));

    ui->viewEdgesAction->setChecked(true);
    ui->viewPointsAction->setChecked(true);
    ui->viewFacesAction->setChecked(true);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::onOpenModelTriggered() {
    QString fileName;
    fileName = QFileDialog::getOpenFileName(
                this,
                tr("Выберите модель"),
                "",
                tr("Файлы объектов (*.obj)"));

    QFile file(fileName);
    if (!file.open(QFile::OpenModeFlag::ReadOnly)) {
        QMessageBox::critical(
                    this,
                    tr("Ошибка"),
                    tr("Нет доступа к файлу"));
        return;
    }
    QTextStream stream(&file);

    QSharedPointer<Model> model = ModelLoader::loadFromObj(stream);
    if (model.isNull()) {
        QMessageBox::critical(
                    this,
                    tr("Ошибка"),
                    tr("Неправильный формат файла"));
        return;
    }

    ui->viewport->setMesh(model->mesh());

    file.close();
}

void MainWindow::onGenerateTetrahedronTriggered() {
    TetrahedronDialog dialog(this);
    int result = dialog.exec();
    if (result != QDialog::DialogCode::Accepted)
        return;
    QSharedPointer<Mesh> mesh = dialog.generatedMesh();
    if (mesh) {
        ui->viewport->setMesh(mesh);
    }
}

void MainWindow::onGenerateSphereTriggered() {
    SphereDialog dialog(this);
    int result = dialog.exec();
    if (result != QDialog::DialogCode::Accepted)
        return;
    QSharedPointer<Mesh> mesh = dialog.generatedMesh();
    if (mesh) {
        ui->viewport->setMesh(mesh);
    }
}

void MainWindow::onGenerateEllipsoidTriggered() {
    EllipsoidDialog dialog(this);
    int result = dialog.exec();
    if (result != QDialog::DialogCode::Accepted)
        return;
    QSharedPointer<Mesh> mesh = dialog.generatedMesh();
    if (mesh) {
        ui->viewport->setMesh(mesh);
    }
}

void MainWindow::onGenerateCubeTriggered() {
    CubeDialog dialog(this);
    int result = dialog.exec();
    if (result != QDialog::DialogCode::Accepted)
        return;
    QSharedPointer<Mesh> mesh = dialog.generatedMesh();
    if (mesh) {
        ui->viewport->setMesh(mesh);
    }
}

void MainWindow::onGenerateDecartFunctionTriggered() {
    DecartFunctionDialog dialog(this);
    int result = dialog.exec();
    if (result != QDialog::DialogCode::Accepted)
        return;
    QSharedPointer<Mesh> mesh = dialog.generatedMesh();
    if (mesh) {
        ui->viewport->setMesh(mesh);
    }
}

void MainWindow::onGenerateParametrizedSurfaceTriggered() {
    ParametrizedSurfaceDialog dialog(this);
    int result = dialog.exec();
    if (result != QDialog::DialogCode::Accepted)
        return;
    QSharedPointer<Mesh> mesh = dialog.generatedMesh();
    if (mesh) {
        ui->viewport->setMesh(mesh);
    }
}

void MainWindow::onGeneratePolarSurfaceTriggered() {
    PolarSurfaceDialog dialog(this);
    int result = dialog.exec();
    if (result != QDialog::DialogCode::Accepted)
        return;
    QSharedPointer<Mesh> mesh = dialog.generatedMesh();
    if (mesh) {
        ui->viewport->setMesh(mesh);
    }
}

void MainWindow::onGenerateTorusTriggered() {
    TorusDialog dialog(this);
    int result = dialog.exec();
    if (result != QDialog::DialogCode::Accepted)
        return;
    QSharedPointer<Mesh> mesh = dialog.generatedMesh();
    if (mesh) {
        ui->viewport->setMesh(mesh);
    }
}

void MainWindow::onEdgesToggled(bool state) {
    ui->viewport->setViewEdges(state);
}

void MainWindow::onPolygonsToggled(bool state) {
    ui->viewport->setViewPolygons(state);
}

void MainWindow::onPointsToggled(bool state) {
    ui->viewport->setViewPoints(state);
}

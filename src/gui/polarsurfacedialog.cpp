#include "include/gui/polarsurfacedialog.h"
#include "ui_polarsurfacedialog.h"

#include <QDoubleValidator>
#include <QMessageBox>
#include <ast_node.h>
#include <parser.h>

#include <mesh_generators/polar_surface_generator.h>

PolarSurfaceDialog::PolarSurfaceDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PolarSurfaceDialog)
{
    ui->setupUi(this);
}

PolarSurfaceDialog::~PolarSurfaceDialog()
{
    delete ui;
}

QSharedPointer<Mesh> PolarSurfaceDialog::generatedMesh() {
    return m_mesh;
}

void PolarSurfaceDialog::on_generateButton_clicked() {
    QIntValidator latitude_subdivide_level_validator(2, 50);
    QIntValidator longitude_subdivide_level_validator(2, 50);

    int pos = 0;

    QString radius_function_string = ui->radiusFunctionEdit->text();
    QString latitude_subdivide_level_string = ui->latitudeEdit->text();
    QString longitude_subdivide_level_string = ui->longitudeEdit->text();

    if (latitude_subdivide_level_validator.validate(latitude_subdivide_level_string, pos) != QValidator::State::Acceptable) {
        QMessageBox::critical(this, "Ошибка",
                              "Поле детализации по широте с содержит некорректные данные");
        return;
    }
    if (longitude_subdivide_level_validator.validate(longitude_subdivide_level_string, pos) != QValidator::State::Acceptable) {
        QMessageBox::critical(this, "Ошибка",
                              "Поле детализации по долготе с содержит некорректные данные");
        return;
    }

    unsigned int latitude_subdivide_level = latitude_subdivide_level_string.toUInt();
    unsigned int longitude_subdivide_level = longitude_subdivide_level_string.toUInt();

    QSharedPointer<ASTNode> function_r = Parser::parse(radius_function_string);
    if (!function_r) {
        QMessageBox::critical(this, "Ошибка",
                              "Поле функция радиуса содержит некорректные данные.");
        return;
    }

    PolarSurfaceGenerator generator;
    generator.function_r() = function_r;
    generator.lattitude_subdivision_level() = latitude_subdivide_level;
    generator.longitude_subdivision_level() = longitude_subdivide_level;

    m_mesh = generator.generate();

    if (!m_mesh) {
        QMessageBox::critical(this, "Ошибка",
                              "Заданная функция некорректна.");
        return;
    }

    accept();
}

void PolarSurfaceDialog::on_cancelButton_clicked() {
    reject();
}

#include "include/gui/ellipsoiddialog.h"
#include "ui_ellipsoiddialog.h"

#include "mesh_transformers/transformation_transformer.h"
#include <QIntValidator>
#include <QMessageBox>
#include <mesh_generators/uvsphere_generator.h>

EllipsoidDialog::EllipsoidDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EllipsoidDialog)
{
    ui->setupUi(this);
}

EllipsoidDialog::~EllipsoidDialog()
{
    delete ui;
}

QSharedPointer<Mesh> EllipsoidDialog::generatedMesh() {
    QSharedPointer<Mesh> mesh;

    UVSphereGenerator generator(1.0);
    generator.lattitude_subdivision() = m_latitude_subdivision;
    generator.longitude_subdivision() = m_longitude_subdivision;
    mesh = generator.generate();

    TransformationTransformer transformer;

    QMatrix4x4 matrix;
    matrix.scale(m_radius, m_height, m_radius);

    transformer.transformation() = matrix;
    mesh = transformer.transform(mesh);
    return mesh;
}

void EllipsoidDialog::on_generateButton_clicked() {
    QDoubleValidator height_validator(1.0, 100.0, 5);
    QDoubleValidator radius_validator(1.0, 100.0, 5);
    QIntValidator latitude_subdivision_validator(2, 50);
    QIntValidator longitude_subdivision_validator(2, 50);

    int pos;
    QString height_string = ui->heightEdit->text();
    QString radius_string = ui->radiusEdit->text();
    QString latitude_subdivision_string = ui->latitudeEdit->text();
    QString longitude_subdivision_string = ui->longitudeEdit->text();

    if (height_validator.validate(height_string, pos) != QValidator::State::Acceptable) {
        QMessageBox::critical(this, "Ошибка",
                              "Поле высоты содержит некорректные данные");
        return;
    }
    if (radius_validator.validate(radius_string, pos) != QValidator::State::Acceptable) {
        QMessageBox::critical(this, "Ошибка",
                              "Поле радиуса содержит некорректные данные");
        return;
    }
    if (latitude_subdivision_validator.validate(latitude_subdivision_string, pos) != QValidator::State::Acceptable) {
        QMessageBox::critical(this, "Ошибка",
                              "Поле детализации по широте содержит некорректные данные");
        return;
    }
    if (longitude_subdivision_validator.validate(longitude_subdivision_string, pos) != QValidator::State::Acceptable) {
        QMessageBox::critical(this, "Ошибка",
                              "Поле детализации по долготе содержит некорректные данные");
        return;
    }

    m_height = height_string.toFloat();
    m_radius = radius_string.toFloat();
    m_latitude_subdivision = latitude_subdivision_string.toUInt();
    m_longitude_subdivision = longitude_subdivision_string.toUInt();

    accept();
    return;
}

void EllipsoidDialog::on_cancelButton_clicked() {
    reject();
}

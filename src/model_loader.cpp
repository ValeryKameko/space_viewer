#include "model_loader.h"
#include "meshes/polygon_mesh.h"
#include "meshes/composite_mesh.h"
#include <QDir>
#include <QFile>
#include <QTextStreamManipulator>
#include <QVector2D>
#include <QVector3D>
#include <QVector4D>
#include <QDebug>

ModelLoader::ModelLoader() {

}

ModelLoader::~ModelLoader() {

}

QSharedPointer<QVector<int>> read_face_id(QTextStream &stream) {
    QSharedPointer<QVector<int>> face(new QVector<int>());
    int vertex_id, texture_coordinate_id = 0, normal_id = 0;

    stream >> vertex_id;
    if (stream.status() != QTextStream::Ok) {
        stream.setStatus(QTextStream::Ok);
        stream.resetStatus();
        return nullptr;
    }
    QChar ch;
    stream >> ch;
    if (stream.status() != QTextStream::Ok || ch == ' ') {
        stream.setStatus(QTextStream::Ok);
        stream.resetStatus();

        face->append(vertex_id);
        face->append(texture_coordinate_id);
        face->append(normal_id);
        return face;
    }
    if (ch != QChar('/'))
        return nullptr;

    stream >> texture_coordinate_id;
    if (stream.status() != QTextStream::Ok) {
        stream.setStatus(QTextStream::Ok);
        stream.resetStatus();

        texture_coordinate_id = 0;
        stream >> ch;
        if (ch != QChar('/'))
            return nullptr;
        stream >> normal_id;

        face->append(vertex_id);
        face->append(texture_coordinate_id);
        face->append(normal_id);
        return face;
    }

    stream >> ch;
    if (ch != QChar('/'))
        return nullptr;
    stream >> normal_id;

    face->append(vertex_id);
    face->append(texture_coordinate_id);
    face->append(normal_id);
    return face;

}

QSharedPointer<Model> ModelLoader::loadFromObj(QTextStream &obj_content) {
    QVector<QVector3D> vertices;
    QVector<QVector3D> normals;
    QVector<QVector2D> texture_coordinates;
    QVector<QVector<QVector<int> > > faces;

    while (!obj_content.atEnd()) {
        QString line = obj_content.readLine();
        QTextStream line_stream(&line, QIODevice::OpenModeFlag::ReadOnly);

        QString header;
        line_stream >> header;
        if (header.length() == 0 || header == "#")
            continue;
        if (header == "v") {
            QVector<float> vector;
            while (true) {
                float coord;
                line_stream >> coord;
                if (line_stream.status() != QTextStream::Ok)
                    break;
                vector.append(coord);
            }
            if (vector.size() == 3) {
                vertices.append(QVector3D(vector[0], vector[1], vector[2]));
            } else if (vector.size() == 4) {
                vertices.append(QVector4D(vector[0], vector[1], vector[2], vector[3]).toVector3DAffine());
            } else {
                return nullptr;
            }
        } else if (header == "vn") {
            QVector<float> vector;
            while (true) {
                float coord;
                line_stream >> coord;
                if (line_stream.status() != QTextStream::Ok)
                    break;
                vector.append(coord);
            }
            if (vector.size() == 3) {
                normals.append(QVector3D(vector[0], vector[1], vector[2]));
            } else if (vector.size() == 4) {
                normals.append(QVector4D(vector[0], vector[1], vector[2], vector[3]).toVector3DAffine());
            } else {
                return nullptr;
            }
        } else if (header == "vt") {
            QVector<float> vector;
            while (true) {
                float coord;
                line_stream >> coord;
                if (line_stream.status() != QTextStream::Ok)
                    break;
                vector.append(coord);
            }
            if (vector.size() == 2) {
                texture_coordinates.append(QVector2D(vector[0], vector[1]));
            } else if (vector.size() == 3){
                texture_coordinates.append(QVector2D(vector[0] / vector[2], vector[1] / vector[2]));
            } else {
                return nullptr;
            }
        } else if (header == "f") {
            QVector<QVector<int>> face;
            while (true) {
                QSharedPointer<QVector<int>> face_id;
                face_id = read_face_id(line_stream);
                if (!face_id)
                    break;

                face.append(*face_id);
            }
            faces.append(face);
        }
    }

    QSharedPointer<Model> model(new Model());
    QSharedPointer<CompositeMesh> mesh(new CompositeMesh());

    for (const QVector<QVector<int>> &face : faces) {
        QSharedPointer<PolygonMesh> polygon(new PolygonMesh());
        for (const QVector<int> &face_id : face) {
            polygon->vertices().append(vertices[face_id[0] - 1]);
            if (face_id[1] > 0)
                polygon->vertices().append(texture_coordinates[face_id[1] - 1]);
            if (face_id[2] > 0)
                polygon->normals().append(normals[face_id[2] - 1]);
        }
        mesh->meshes().append(polygon);
    }
    model->mesh() = mesh;
    return model;
}


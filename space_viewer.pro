#-------------------------------------------------
#
# Project created by QtCreator 2018-11-25T17:15:50
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = space_viewer
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

CONFIG += c++11

INCLUDEPATH += include

SOURCES += \
    src/gui/mainwindow.cpp \
    src/gui/viewport.cpp \
    src/materials/color_material.cpp \
    src/materials/material.cpp \
    src/mesh_generators/cube_generator.cpp \
    src/renderers/wireframe_renderer.cpp \
    src/camera.cpp \
    src/main.cpp \
    src/mesh_generator.cpp \
    src/model.cpp \
    src/renderer.cpp \
    src/mesh.cpp \
    src/meshes/line_mesh.cpp \
    src/meshes/composite_mesh.cpp \
    src/meshes/polygon_mesh.cpp \
    src/mesh_generators/torus_generator.cpp \
    src/mesh_generators/uvsphere_generator.cpp \
    src/meshes/triangle_mesh.cpp \
    src/mesh_transformers/quad_subdivider_transformer.cpp \
    src/mesh_transformers/triangle_subdivider_transformer.cpp \
    src/mesh_transformer.cpp \
    src/meshes/quad_mesh.cpp \
    src/mesh_generators/tetrahedron_generator.cpp \
    src/mesh_transformers/spherify_transformer.cpp \
    src/mesh_transformers/transformation_transformer.cpp \
    src/mesh_generators/pyramid_generator.cpp \
    src/mesh_function.cpp \
    src/mesh_functions/foreach_vertex_function.cpp \
    src/meshes/point_mesh.cpp \
    src/model_loader.cpp \
    src/renderers/point_renderer.cpp \
    src/ast_node.cpp \
    src/ast_nodes/ast_constant_node.cpp \
    src/ast_nodes/ast_function_node.cpp \
    src/ast_nodes/ast_variable_node.cpp \
    src/renderers/polygon_renderer.cpp \
    src/parser.cpp \
    src/z_axis_function_generator.cpp \
    src/gui/tetrahedrondialog.cpp \
    src/gui/spheredialog.cpp \
    src/gui/ellipsoiddialog.cpp \
    src/gui/cubedialog.cpp \
    src/gui/decartfunctiondialog.cpp \
    src/mesh_generators/parametrized_surface_generator.cpp \
    src/gui/parametrizedsurfacedialog.cpp \
    src/mesh_generators/polar_surface_generator.cpp \
    src/gui/polarsurfacedialog.cpp \
    src/gui/torusdialog.cpp

HEADERS += \
    include/gui/mainwindow.h \
    include/gui/viewport.h \
    include/materials/color_material.h \
    include/mesh_generators/cube_generator.h \
    include/renderers/wireframe_renderer.h \
    include/camera.h \
    include/material.h \
    include/mesh.h \
    include/mesh_generator.h \
    include/model.h \
    include/renderer.h \
    include/meshes/line_mesh.h \
    include/meshes/composite_mesh.h \
    include/mesh_generators/torus_generator.h \
    include/mesh_generators/uvsphere_generator.h \
    include/mesh_transformer.h \
    include/mesh_transformers/quad_subdivider_transformer.h \
    include/mesh_transformers/triangle_subdivider_transformer.h \
    include/mesh_generators/tetrahedron_generator.h \
    include/mesh_transformers/transformation_transformer.h \
    include/mesh_transformers/spherify_transformer.h \
    include/mesh_generators/pyramid_generator.h \
    include/mesh_function.h \
    include/mesh_functions/foreach_vertex_function.h \
    include/meshes/point_mesh.h \
    include/meshes/polygon_mesh.h \
    include/meshes/triangle_mesh.h \
    include/meshes/quad_mesh.h \
    include/model_loader.h \
    include/renderers/point_renderer.h \
    include/ast_nodes/ast_constant_node.h \
    include/ast_node.h \
    include/ast_nodes/ast_function_node.h \
    include/ast_nodes/ast_variable_node.h \
    include/renderers/polygon_renderer.h \
    include/parser.h \
    include/mesh_generators/z_axis_function_generator.h \
    include/gui/tetrahedrondialog.h \
    include/gui/spheredialog.h \
    include/gui/ellipsoiddialog.h \
    include/gui/cubedialog.h \
    include/gui/decartfunctiondialog.h \
    include/mesh_generators/parametrized_surface_generator.h \
    include/gui/parametrizedsurfacedialog.h \
    include/mesh_generators/polar_surface_generator.h \
    include/gui/polarsurfacedialog.h \
    include/gui/torusdialog.h

DISTFILES += \
    resources/shaders/wireframe_shader.frag \
    resources/shaders/wireframe_shader.vert \
    resources/shaders/point_shader.vert \
    resources/shaders/point_shader.frag \
    resources/shaders/polygon_shader.vert \
    resources/shaders/polygon_shader.frag

copydata.commands = $(COPY_DIR) "\"$$PWD/resources\"" "\"$$OUT_PWD\""
first.depends = $(first) copydata
export(first.depends)
export(copydata.commands)
QMAKE_EXTRA_TARGETS += first copydata

FORMS += \
    forms/mainwindow.ui \
    forms/tetrahedrondialog.ui \
    forms/spheredialog.ui \
    forms/ellipsoiddialog.ui \
    forms/cubedialog.ui \
    forms/decartfunctiondialog.ui \
    forms/parametrizedsurfacedialog.ui \
    forms/polarsurfacedialog.ui \
    forms/torusdialog.ui

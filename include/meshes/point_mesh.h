#ifndef POINT_MESH_H
#define POINT_MESH_H

#include <QVector>
#include <QVector3D>
#include <QVector2D>
#include <QMap>
#include <QSharedPointer>
#include "mesh.h"

class PointMesh : public Mesh {
public:
    explicit PointMesh();
    virtual ~PointMesh() override;

    const QVector3D &vertex() const;
    QVector3D &vertex();
private:
    QVector3D m_vertex;
};

#endif // POINT_MESH_H

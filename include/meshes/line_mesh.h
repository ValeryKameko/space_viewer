#ifndef LINE_MESH_H
#define LINE_MESH_H

#include <QVector>
#include <QVector3D>
#include <QMap>
#include <QSharedPointer>
#include "mesh.h"

class LineMesh : public Mesh {
public:
    explicit LineMesh();
    virtual ~LineMesh() override;

    const QVector<QVector3D> &vertices() const;

    QVector<QVector3D> &vertices();

    const size_t &line_count() const;
    size_t &line_count();
private:
    QVector<QVector3D> m_vertices;
    size_t m_line_count;
};

#endif // LINE_MESH_H

#ifndef TRIANGLE_MESH_H
#define TRIANGLE_MESH_H

#include "meshes/polygon_mesh.h"

class TriangleMesh : public PolygonMesh {
public:
    explicit TriangleMesh();
    virtual ~TriangleMesh() override;
};

#endif // TRIANGLE_MESH_H

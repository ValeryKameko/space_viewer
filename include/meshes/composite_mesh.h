#ifndef COMPOSITE_MESH_H
#define COMPOSITE_MESH_H

#include <QVector>
#include <QVector3D>
#include <QMap>
#include <QSharedPointer>
#include "mesh.h"

class CompositeMesh : public Mesh {
public:
    explicit CompositeMesh();
    virtual ~CompositeMesh() override;

    const QVector<QSharedPointer<Mesh>> &meshes() const;

    QVector<QSharedPointer<Mesh>> &meshes();
private:
    QVector<QSharedPointer<Mesh>> m_meshes;
};

#endif // COMPOSITE_MESH_H

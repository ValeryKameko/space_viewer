#ifndef POLYGON_MESH_H
#define POLYGON_MESH_H

#include "mesh.h"
#include <QVector>
#include <QVector3D>
#include <QVector2D>

class PolygonMesh : public Mesh {
public:
    PolygonMesh();
    virtual ~PolygonMesh() override;

    const QVector<QVector3D> &vertices() const;
    QVector<QVector3D> &vertices();

    const QVector<QVector2D> &texture_coordinates() const;
    QVector<QVector2D> &texture_coordinates();

    const QVector<QVector3D> &normals() const;
    QVector<QVector3D> &normals();

private:
    QVector<QVector3D> m_normals;
    QVector<QVector2D> m_texture_coordinates;
    QVector<QVector3D> m_vertices;
};

#endif // POLYGON_MESH_H

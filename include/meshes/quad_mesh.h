#ifndef QUAD_MESH_H
#define QUAD_MESH_H

#include "meshes/polygon_mesh.h"

class QuadMesh : public PolygonMesh {
public:
    explicit QuadMesh();
    virtual ~QuadMesh() override;
};

#endif // QUAD_MESH_H

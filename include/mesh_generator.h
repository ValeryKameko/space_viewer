#ifndef MESH_GENERATOR_H
#define MESH_GENERATOR_H

#include "mesh.h"

class MeshGenerator {
public:
    explicit MeshGenerator();
    virtual ~MeshGenerator();

    virtual QSharedPointer<Mesh> generate() = 0;
};

#endif // MESH_GENERATOR_H

#ifndef RENDERER_H
#define RENDERER_H

#include <QMatrix4x4>
#include "model.h"
#include "camera.h"
#include <QSharedPointer>
#include "QOpenGLContext"

class Renderer {
public:
    explicit Renderer();
    virtual ~Renderer();

    virtual void initialize() = 0;
    virtual void render() = 0;
    virtual void uninitialize() = 0;

    QOpenGLContext * context();
    void setContext(QOpenGLContext * context);

    const QSharedPointer<Camera> &camera() const;
    const QMatrix4x4 &model_matrix() const;
    const QSharedPointer<Model> &model() const;

    QSharedPointer<Camera> &camera();
    QMatrix4x4 &model_matrix();
    QSharedPointer<Model> &model();
protected:
    QOpenGLContext * m_context;

    QSharedPointer<Camera> m_camera;
    QMatrix4x4 m_model_matrix;
    QSharedPointer<Model> m_model;
};

#endif // RENDERER_H

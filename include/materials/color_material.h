#ifndef COLOR_MATERIAL_H
#define COLOR_MATERIAL_H

#include "material.h"
#include <QColor>

class ColorMaterial : public Material {
public:
    explicit ColorMaterial(const QColor &color = QColor(Qt::GlobalColor::black));
    virtual ~ColorMaterial() override;
    const QColor &color() const;
    QColor &color();
private:
    QColor m_color;
};

#endif // COLOR_MATERIAL_H

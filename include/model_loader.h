#ifndef MODEL_LOADER_H
#define MODEL_LOADER_H

#include "model.h"
#include "mesh.h"
#include <QTextStream>

class ModelLoader {
public:
    explicit ModelLoader();
    ~ModelLoader();

    static QSharedPointer<Model> loadFromObj(QTextStream &obj_content);
};

#endif // MESH_LOADER_H

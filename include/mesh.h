#ifndef MESH_H
#define MESH_H

#include <QVector>
#include <QMap>
#include <QSharedPointer>

class Mesh {
public:
    explicit Mesh();
    virtual ~Mesh();
};

#endif // MESH_H

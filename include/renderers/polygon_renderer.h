#ifndef POLYGON_RENDERER_H
#define POLYGON_RENDERER_H

#include "renderer.h"
#include "model.h"
#include <QOpenGLShaderProgram>
#include <QOpenGLContext>
#include "meshes/composite_mesh.h"
#include "meshes/polygon_mesh.h"
#include "meshes/line_mesh.h"
#include "meshes/triangle_mesh.h"
#include "mesh.h"

class PolygonRenderer : public Renderer {
public:
    explicit PolygonRenderer();
    virtual ~PolygonRenderer() override;

    virtual void initialize() override;
    virtual void render() override;
    virtual void uninitialize() override;

private:
    QOpenGLShaderProgram m_program;

    void renderMesh(const QSharedPointer<Mesh> &mesh);
    void renderCompositeMesh(const QSharedPointer<CompositeMesh> &mesh);
    void renderPolygonMesh(const QSharedPointer<PolygonMesh> &mesh);
};

#endif // POLYGON_RENDERER_H

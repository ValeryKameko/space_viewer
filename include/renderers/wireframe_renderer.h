#ifndef WIREFRAME_RENDERER_H
#define WIREFRAME_RENDERER_H

#include "renderer.h"
#include "model.h"
#include <QOpenGLShaderProgram>
#include <QOpenGLContext>
#include "meshes/composite_mesh.h"
#include "meshes/polygon_mesh.h"
#include "meshes/line_mesh.h"
#include "meshes/triangle_mesh.h"
#include "mesh.h"

class WireframeRenderer : public Renderer {
public:
    explicit WireframeRenderer();
    virtual ~WireframeRenderer() override;

    virtual void initialize() override;
    virtual void render() override;
    virtual void uninitialize() override;
private:
    QOpenGLShaderProgram m_program;

    void renderMesh(const QSharedPointer<Mesh> &mesh);
    void renderLineMesh(const QSharedPointer<LineMesh> &mesh);
    void renderCompositeMesh(const QSharedPointer<CompositeMesh> &mesh);
    void renderPolygonMesh(const QSharedPointer<PolygonMesh> &mesh);
};

#endif // WIREFRAME_RENDERER_H

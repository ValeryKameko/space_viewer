#ifndef POINT_RENDERER_H
#define POINT_RENDERER_H

#include "renderer.h"
#include "meshes/composite_mesh.h"
#include "meshes/line_mesh.h"
#include "meshes/polygon_mesh.h"
#include "meshes/point_mesh.h"
#include <QColor>
#include <QOpenGLShaderProgram>

class PointRenderer : public Renderer {
public:
    explicit PointRenderer();
    virtual ~PointRenderer() override;

    virtual void initialize() override;
    virtual void render() override;
    virtual void uninitialize() override;

    const float &point_size() const;
    float &point_size();
private:
    QOpenGLShaderProgram m_program;
    float m_point_size;

    void renderMesh(const QSharedPointer<Mesh> &mesh);
    void renderLineMesh(const QSharedPointer<LineMesh> &mesh);
    void renderCompositeMesh(const QSharedPointer<CompositeMesh> &mesh);
    void renderPolygonMesh(const QSharedPointer<PolygonMesh> &mesh);
    void renderPointMesh(const QSharedPointer<PointMesh> &mesh);
};

#endif // POINT_RENDERER_H

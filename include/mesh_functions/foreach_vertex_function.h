#ifndef FOREACH_VERTEX_FUNCTION_H
#define FOREACH_VERTEX_FUNCTION_H

#include "mesh_function.h"
#include "mesh.h"
#include "meshes/polygon_mesh.h"
#include "meshes/composite_mesh.h"
#include <QVector3D>
#include <QSharedPointer>

class ForeachVertexFunction : MeshFunction {
public:
    explicit ForeachVertexFunction();
    virtual ~ForeachVertexFunction();

    void run(const QSharedPointer<Mesh> &mesh, const std::function<void(const QVector3D &vertex)> &func);
private:
    void runCompositeMesh(const QSharedPointer<CompositeMesh> &mesh, const std::function<void(const QVector3D &vertex)> &func);
    void runPolygonMesh(const QSharedPointer<PolygonMesh> &mesh, const std::function<void(const QVector3D &vertex)> &func);
};

#endif // FOREACH_VERTEX_FUNCTION_H

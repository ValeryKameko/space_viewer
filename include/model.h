#ifndef MODEL_H
#define MODEL_H

#include "mesh.h"
#include "material.h"

class Model {
public:
    explicit Model();
    virtual ~Model();

    const QSharedPointer<Mesh> &mesh() const;
    const QSharedPointer<Material> &material() const;

    QSharedPointer<Mesh> &mesh();
    QSharedPointer<Material> &material();
protected:
    QSharedPointer<Mesh> m_mesh;
    QSharedPointer<Material> m_material;
};

#endif // MODEL_H

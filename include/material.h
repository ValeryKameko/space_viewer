#ifndef MATERIAL_H
#define MATERIAL_H

#include <QMap>
#include <QSharedPointer>

class Material {
public:
    explicit Material();
    virtual ~Material();
};

#endif // MATERIAL_H

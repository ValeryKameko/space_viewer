#ifndef SPHERIFY_TRANSFORMER_H
#define SPHERIFY_TRANSFORMER_H

#include "mesh_transformer.h"
#include "meshes/composite_mesh.h"
#include "meshes/triangle_mesh.h"
#include "meshes/quad_mesh.h"
#include "meshes/polygon_mesh.h"
#include "mesh.h"

class SpherifyTransformer : MeshTransformer {
public:
    explicit SpherifyTransformer(float radius = 1.0);
    virtual ~SpherifyTransformer() override;

    virtual QSharedPointer<Mesh> transform(const QSharedPointer<Mesh> &mesh) override;

    const float &radius() const;
    float &radius();
private:
    float m_radius;

    QSharedPointer<Mesh> spherifyTriangleMesh(const QSharedPointer<TriangleMesh> &mesh);
    QSharedPointer<Mesh> spherifyPolygonMesh(const QSharedPointer<PolygonMesh> &mesh);
    QSharedPointer<Mesh> spherifyQuadMesh(const QSharedPointer<QuadMesh> &mesh);
    QSharedPointer<Mesh> spherifyCompositeMesh(const QSharedPointer<CompositeMesh> &mesh);
};

#endif // SPHERIFY_TRANSFORMER_H

#ifndef TRIANGLE_SUBDIVIDER_TRANSFORMER_H
#define TRIANGLE_SUBDIVIDER_TRANSFORMER_H

#include "mesh_transformer.h"
#include "meshes/polygon_mesh.h"
#include "meshes/triangle_mesh.h"
#include "meshes/composite_mesh.h"

class TriangleSubdividerTransformer : MeshTransformer {
public:
    explicit TriangleSubdividerTransformer(unsigned int subdivision_level = 1);
    virtual ~TriangleSubdividerTransformer() override;

    virtual QSharedPointer<Mesh> transform(const QSharedPointer<Mesh> &mesh) override;

    const unsigned int &subdivision_level() const;
    unsigned int &subdivision_level();

private:
    unsigned int m_subdivision_level;
    QSharedPointer<Mesh> subdivideTriangleMesh(const QSharedPointer<TriangleMesh> &mesh);
    QSharedPointer<Mesh> subdivideCompositeMesh(const QSharedPointer<CompositeMesh> &mesh);
};

#endif // TRIANGLE_SUBDIVIDER_TRANSFORMER_H

#ifndef QUAD_SUBDIVIDER_TRANSFORMER_H
#define QUAD_SUBDIVIDER_TRANSFORMER_H

#include "mesh_transformer.h"
#include "meshes/composite_mesh.h"
#include "meshes/quad_mesh.h"

class QuadSubdividerTransformer : MeshTransformer {
public:
    explicit QuadSubdividerTransformer(unsigned int subdivision_count = 2);
    virtual ~QuadSubdividerTransformer() override;

    virtual QSharedPointer<Mesh> transform(const QSharedPointer<Mesh> &mesh) override;

    const unsigned int &subdivision_count() const;
    unsigned int &subdivision_count();
private:
    unsigned int m_subdivision_count;
    QSharedPointer<Mesh> subdivideQuadMesh(const QSharedPointer<QuadMesh> &mesh);
    QSharedPointer<Mesh> subdivideCompositeMesh(const QSharedPointer<CompositeMesh> &mesh);
    QVector3D subdivisionPoint(const QSharedPointer<QuadMesh> &mesh,
                               unsigned int i,
                               unsigned int j);
};

#endif // QUAD_SUBDIVIDER_TRANSFORMER_H

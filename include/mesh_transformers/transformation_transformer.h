#ifndef TRANSFORMATION_TRANSFORMER_H
#define TRANSFORMATION_TRANSFORMER_H

#include "mesh_transformer.h"
#include "meshes/polygon_mesh.h"
#include "meshes/triangle_mesh.h"
#include "meshes/quad_mesh.h"
#include "meshes/composite_mesh.h"
#include <QMatrix4x4>

class TransformationTransformer : MeshTransformer {
public:
    explicit TransformationTransformer(const QMatrix4x4 &transformation = QMatrix4x4());
    virtual ~TransformationTransformer() override;

    virtual QSharedPointer<Mesh> transform(const QSharedPointer<Mesh> &mesh) override;

    const QMatrix4x4 &transformation() const;
    QMatrix4x4 &transformation();
private:
    QMatrix4x4 m_transformation;
    QSharedPointer<Mesh> transformTriangleMesh(const QSharedPointer<TriangleMesh> &mesh);
    QSharedPointer<Mesh> transformPolygonMesh(const QSharedPointer<PolygonMesh> &mesh);
    QSharedPointer<Mesh> transformQuadMesh(const QSharedPointer<QuadMesh> &mesh);
    QSharedPointer<Mesh> transformCompositeMesh(const QSharedPointer<CompositeMesh> &mesh);
};

#endif // TRANSFORMATION_TRANSFORMER_H

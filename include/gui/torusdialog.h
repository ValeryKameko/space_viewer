#ifndef TORUSDIALOG_H
#define TORUSDIALOG_H

#include <QDialog>
#include <mesh.h>

namespace Ui {
class TorusDialog;
}

class TorusDialog : public QDialog
{
    Q_OBJECT

public:
    explicit TorusDialog(QWidget *parent = nullptr);
    ~TorusDialog();

    QSharedPointer<Mesh> generatedMesh();

private slots:
    void on_generateButton_clicked();

    void on_cancelButton_clicked();

private:
    Ui::TorusDialog *ui;
    QSharedPointer<Mesh> m_mesh;
};

#endif // TORUSDIALOG_H

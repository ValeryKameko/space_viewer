#ifndef CUBEDIALOG_H
#define CUBEDIALOG_H

#include <QDialog>
#include <mesh.h>

namespace Ui {
class CubeDialog;
}

class CubeDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CubeDialog(QWidget *parent = nullptr);
    ~CubeDialog();

    QSharedPointer<Mesh> generatedMesh();

private slots:
    void on_generateButton_clicked();
    void on_cancelButton_clicked();

private:
    Ui::CubeDialog *ui;
    float m_size;
    float m_subdivision;
};

#endif // CUBEDIALOG_H

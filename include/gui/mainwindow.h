#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void onOpenModelTriggered();
    void onGenerateTetrahedronTriggered();
    void onGenerateSphereTriggered();
    void onGenerateEllipsoidTriggered();
    void onGenerateCubeTriggered();
    void onGenerateDecartFunctionTriggered();
    void onGenerateParametrizedSurfaceTriggered();
    void onGeneratePolarSurfaceTriggered();
    void onGenerateTorusTriggered();

    void onEdgesToggled(bool state);
    void onPolygonsToggled(bool state);
    void onPointsToggled(bool state);
private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H

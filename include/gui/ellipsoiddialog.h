#ifndef ELLIPSOIDDIALOG_H
#define ELLIPSOIDDIALOG_H

#include <QDialog>
#include <mesh.h>

namespace Ui {
class EllipsoidDialog;
}

class EllipsoidDialog : public QDialog
{
    Q_OBJECT

public:
    explicit EllipsoidDialog(QWidget *parent = nullptr);
    ~EllipsoidDialog();


    QSharedPointer<Mesh> generatedMesh();
private slots:
    void on_generateButton_clicked();
    void on_cancelButton_clicked();

private:
    Ui::EllipsoidDialog *ui;

    float m_height;
    float m_radius;
    unsigned int m_latitude_subdivision;
    unsigned int m_longitude_subdivision;
};

#endif // ELLIPSOIDDIALOG_H

#ifndef PARAMETRIZEDSURFACEDIALOG_H
#define PARAMETRIZEDSURFACEDIALOG_H

#include <QDialog>
#include <ast_node.h>
#include <mesh.h>

namespace Ui {
class ParametrizedSurfaceDialog;
}

class ParametrizedSurfaceDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ParametrizedSurfaceDialog(QWidget *parent = nullptr);
    ~ParametrizedSurfaceDialog();

    QSharedPointer<Mesh> generatedMesh();

private slots:
    void on_generate_button_clicked();
    void on_cancel_button_clicked();

private:
    Ui::ParametrizedSurfaceDialog *ui;

    float m_min_s;
    float m_max_s;
    float m_min_t;
    float m_max_t;

    unsigned int m_subdivision_s;
    unsigned int m_subdivision_t;

    QSharedPointer<ASTNode> m_function_x;
    QSharedPointer<ASTNode> m_function_y;
    QSharedPointer<ASTNode> m_function_z;
    QSharedPointer<Mesh> m_mesh;
};

#endif // PARAMETRIZEDSURFACEDIALOG_H

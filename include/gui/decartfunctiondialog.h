#ifndef DECARTFUNCTIONDIALOG_H
#define DECARTFUNCTIONDIALOG_H

#include <QDialog>
#include <ast_node.h>
#include <mesh.h>

namespace Ui {
class DecartFunctionDialog;
}

class DecartFunctionDialog : public QDialog
{
    Q_OBJECT

public:
    explicit DecartFunctionDialog(QWidget *parent = nullptr);
    ~DecartFunctionDialog();


    QSharedPointer<Mesh> generatedMesh();
private slots:
    void on_generateButton_clicked();
    void on_cancelButton_clicked();

private:
    Ui::DecartFunctionDialog *ui;

    float m_min_x, m_max_x;
    float m_min_y, m_max_y;

    unsigned int m_subdivision_x;
    unsigned int m_subdivision_y;

    QSharedPointer<ASTNode> m_function;

    QSharedPointer<Mesh> m_mesh;
};

#endif // DECARTFUNCTIONDIALOG_H

#ifndef POLARSURFACEDIALOG_H
#define POLARSURFACEDIALOG_H

#include <QDialog>
#include <mesh.h>

namespace Ui {
class PolarSurfaceDialog;
}

class PolarSurfaceDialog : public QDialog
{
    Q_OBJECT

public:
    explicit PolarSurfaceDialog(QWidget *parent = nullptr);
    ~PolarSurfaceDialog();

    QSharedPointer<Mesh> generatedMesh();
private slots:
    void on_generateButton_clicked();

    void on_cancelButton_clicked();

private:
    Ui::PolarSurfaceDialog *ui;

    QSharedPointer<Mesh> m_mesh;
};

#endif // POLARSURFACEDIALOG_H

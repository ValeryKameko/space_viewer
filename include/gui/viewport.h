#ifndef VIEWPORT_H
#define VIEWPORT_H

#include <QObject>
#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QSharedPointer>
#include <QQuaternion>
#include "model.h"
#include "renderers/wireframe_renderer.h"
#include "renderers/point_renderer.h"
#include "renderers/polygon_renderer.h"

class Viewport : public QOpenGLWidget, protected QOpenGLFunctions
{
public:
    Viewport(QWidget * parent = nullptr);
    ~Viewport() override;

    void setMesh(const QSharedPointer<Mesh> &mesh);

    void setWireframeMaterial(const QSharedPointer<Material> &material);
    void setPointMaterial(const QSharedPointer<Material> &material);
    void setPolygonMaterial(const QSharedPointer<Material> &material);

    void setViewEdges(bool state);
    void setViewPolygons(bool state);
    void setViewPoints(bool state);

protected:
    void initializeGL() override;
    void resizeGL(int w, int h) override;
    void paintGL() override;

    void wheelEvent(QWheelEvent * event) override;
    void mousePressEvent(QMouseEvent * event) override;
    void mouseMoveEvent(QMouseEvent * event) override;

private slots:
    void uninitialize();

private:
    enum class GestureType {
        None,
        Rotation,
        Moving
    };

    bool m_viewEdges;
    bool m_viewPolygons;
    bool m_viewPoints;

    QSharedPointer<Model> m_wireframe_model;
    QSharedPointer<Model> m_point_model;
    QSharedPointer<Model> m_polygon_model;

    QSharedPointer<Renderer> m_wireframe_renderer;
    QSharedPointer<Renderer> m_point_renderer;
    QSharedPointer<Renderer> m_polygon_renderer;

    float m_camera_distance;
    QQuaternion m_camera_rotation;
    QVector3D m_camera_center;

    QPoint m_last_position;
    GestureType m_gesture_type;

    QSharedPointer<Camera> m_camera;

    void setMatrices();
};

#endif // VIEWPORT_H

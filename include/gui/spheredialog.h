#ifndef SPHEREDIALOG_H
#define SPHEREDIALOG_H

#include <QDialog>
#include "mesh.h"

namespace Ui {
class SphereDialog;
}

class SphereDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SphereDialog(QWidget *parent = nullptr);
    ~SphereDialog();

    QSharedPointer<Mesh> generatedMesh();

private slots:
    void on_generateButton_clicked();

private:
    Ui::SphereDialog *ui;

    float m_radius;
    unsigned int m_latitude_subdivide_level;
    unsigned int m_longitude_subdivide_level;
};

#endif // SPHEREDIALOG_H

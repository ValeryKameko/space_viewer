#ifndef TETRAHEDRONDIALOG_H
#define TETRAHEDRONDIALOG_H

#include <QDialog>
#include "mesh.h"
#include "mesh_generators/tetrahedron_generator.h"

namespace Ui {
class TetrahedronDialog;
}

class TetrahedronDialog : public QDialog
{
    Q_OBJECT

public:
    explicit TetrahedronDialog(QWidget *parent = nullptr);
    ~TetrahedronDialog();

    QSharedPointer<Mesh> generatedMesh();
private slots:
    void on_generateButton_clicked();
    void on_cancelButton_clicked();

private:
    Ui::TetrahedronDialog *ui;

    float m_size;
    int m_subdivision_level;
};

#endif // TETRAHEDRONDIALOG_H

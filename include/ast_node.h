#ifndef AST_NODE_H
#define AST_NODE_H

#include <QMap>
#include <QString>

class ASTNode {
public:
    explicit ASTNode();
    virtual ~ASTNode();

    virtual float evaluate(const QMap<QString, float> &values) const = 0;
};

#endif // AST_NODE_H

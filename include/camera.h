#ifndef CAMERA_H
#define CAMERA_H

#include <QMatrix4x4>

class Camera {
public:
    explicit Camera();
    ~Camera();

    const QMatrix4x4 &projection_matrix() const;
    const QMatrix4x4 &transformation_matrix() const;

    QMatrix4x4 &projection_matrix();
    QMatrix4x4 &transformation_matrix();
private:
    QMatrix4x4 m_projection_matrix;
    QMatrix4x4 m_transformation_matrix;
};

#endif // CAMERA_H

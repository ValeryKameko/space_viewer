#ifndef PARSER_H
#define PARSER_H

#include "ast_node.h"
#include <QSharedPointer>

class Parser {
public:
    explicit Parser();
    ~Parser();

    static QSharedPointer<ASTNode> parse(const QString &function);
};

#endif // PARSER_H

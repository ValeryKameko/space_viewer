#ifndef AST_CONSTANT_NODE_H
#define AST_CONSTANT_NODE_H

#include "ast_node.h"

class ASTConstantNode : public ASTNode {
public:
    explicit ASTConstantNode(float value = 0.0);
    virtual ~ASTConstantNode() override;

    const float &value() const;
    float &value();

    virtual float evaluate(const QMap<QString, float> &values) const override;
private:
    float m_value;
};

#endif // AST_CONSTANT_NODE_H

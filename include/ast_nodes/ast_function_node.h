#ifndef AST_FUNCTION_NODE_H
#define AST_FUNCTION_NODE_H

#include "ast_node.h"
#include <QString>
#include <QVector>
#include <QSharedPointer>

class ASTFunctionNode : public ASTNode {
public:
    explicit ASTFunctionNode();
    virtual ~ASTFunctionNode() override;

    const QString &function_name() const;
    QString &function_name();

    const QVector<QSharedPointer<ASTNode>> &parameters() const;
    QVector<QSharedPointer<ASTNode>> &parameters();

    virtual float evaluate(const QMap<QString, float> &values) const override;
private:
    QString m_function_name;

    QVector<QSharedPointer<ASTNode>> m_parameters;
};

#endif // AST_FUNCTION_NODE_H

#ifndef AST_VARIABLE_NODE_H
#define AST_VARIABLE_NODE_H

#include <QString>
#include <QMap>
#include "ast_node.h"


class ASTVariableNode : public ASTNode {
public:
    explicit ASTVariableNode(const QString &variable_name = "");
    virtual ~ASTVariableNode() override;

    const QString &variable_name() const;
    QString &variable_name();

    virtual float evaluate(const QMap<QString, float> &values) const override;
private:
    QString m_variable_name;
};

#endif // AST_VARIABLE_NODE_H

#ifndef CUBE_GENERATOR_H
#define CUBE_GENERATOR_H

#include "mesh_generator.h"
#include "mesh.h"

class CubeGenerator : public MeshGenerator {
public:
    explicit CubeGenerator(float size = 1.0);

    virtual QSharedPointer<Mesh> generate() override;

    const float &size() const;
    float &size();
private:
    float m_size;

    QSharedPointer<Mesh> generateFace(const QVector3D &normal);
};

#endif // CUBE_GENERATOR_H

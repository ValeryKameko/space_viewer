#ifndef UVSPHERE__GENERATOR_H
#define UVSPHERE_GENERATOR_H

#include "mesh_generator.h"
#include "mesh.h"

class UVSphereGenerator : public MeshGenerator {
public:
    explicit UVSphereGenerator(float radius = 1.0);

    virtual QSharedPointer<Mesh> generate() override;

    const float &radius() const;
    float &radius();

    const unsigned int &longitude_subdivision() const;
    unsigned int &longitude_subdivision();

    const unsigned int &lattitude_subdivision() const;
    unsigned int &lattitude_subdivision();
private:
    float m_radius;
    unsigned int m_longitude_subdivision;
    unsigned int m_lattitude_subdivision;

    QSharedPointer<Mesh> generateSegment(float left_longitude_angle, float right_longitude_angle);
    QVector3D generateVertex(float longitude_angle, float lattitude_angle);
};

#endif // UVSPHERE_GENERATOR_H

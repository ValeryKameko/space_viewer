#ifndef Z_AXIS_FUNCTION_GENERATOR_H
#define Z_AXIS_FUNCTION_GENERATOR_H

#include "mesh_generator.h"
#include "ast_node.h"

class ZAxisFunctionGenerator : public MeshGenerator {
public:
    explicit ZAxisFunctionGenerator();
    virtual ~ZAxisFunctionGenerator() override;

    QSharedPointer<ASTNode> &function() { return m_function; }
    const QSharedPointer<ASTNode> &function() const  { return m_function; }

    virtual QSharedPointer<Mesh> generate() override;

    float &min_x() { return m_min_x; }
    float &min_y() { return m_min_y; }
    float &max_x() { return m_max_x; }
    float &max_y() { return m_max_y; }

    const float &min_x() const { return m_min_x; }
    const float &min_y() const { return m_min_y; }
    const float &max_x() const { return m_max_x; }
    const float &max_y() const { return m_max_y; }

    unsigned int &x_subdivision_level() { return m_x_subdivision_level; }
    unsigned int &y_subdivision_level() { return m_y_subdivision_level; }

    const unsigned int &x_subdivision_level() const { return m_x_subdivision_level; }
    const unsigned int &y_subdivision_level() const { return m_y_subdivision_level; }
private:
    unsigned int m_x_subdivision_level;
    unsigned int m_y_subdivision_level;

    float m_min_x;
    float m_min_y;
    float m_max_x;
    float m_max_y;

    QSharedPointer<ASTNode> m_function;
};

#endif // Z_AXIS_FUNCTION_GENERATOR_H

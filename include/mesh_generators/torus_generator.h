#ifndef TORUS_GENERATOR_H
#define TORUS_GENERATOR_H

#include "mesh_generator.h"
#include "mesh.h"

class TorusGenerator : public MeshGenerator {
public:
    explicit TorusGenerator(float major_radius = 1.0, float minor_radius = 1.0);

    virtual QSharedPointer<Mesh> generate() override;

    const float &major_radius() const;
    float &major_radius();

    const float &minor_radius() const;
    float &minor_radius();

    const unsigned int &major_subdivision() const;
    unsigned int &major_subdivision();

    const unsigned int &minor_subdivision() const;
    unsigned int &minor_subdivision();
private:
    float m_major_radius;
    float m_minor_radius;
    unsigned int m_minor_subdivision;
    unsigned int m_major_subdivision;

    QSharedPointer<Mesh> generateRing(float left_major_angle, float right_major_angle);
    QSharedPointer<Mesh> generateFace(float left_major_angle, float right_major_angle, float left_minor_angle, float right_minor_angle);
};

#endif // TORUS_GENERATOR_H

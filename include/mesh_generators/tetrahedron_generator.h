#ifndef TETRAHEDRON_GENERATOR_H
#define TETRAHEDRON_GENERATOR_H

#include "mesh.h"
#include "mesh_generator.h"
#include <QSharedPointer>
#include <QVector3D>

class TetrahedronGenerator : public MeshGenerator {
public:
    explicit TetrahedronGenerator(float radius = 1.0);
    virtual ~TetrahedronGenerator() override;

    virtual QSharedPointer<Mesh> generate() override;

    const float &radius() const;
    float &radius();
private:
    float m_radius;

    QSharedPointer<Mesh> generateSegment(float left_longitude_angle, float right_longitude_angle);
    QVector3D generateVertex(float longitude_angle, float lattitude_angle);
};


#endif // TETRAHEDRON_GENERATOR_H

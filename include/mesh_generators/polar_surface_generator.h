#ifndef POLAR_SURFACE_GENERATOR_H
#define POLAR_SURFACE_GENERATOR_H

#include <ast_node.h>
#include <mesh_generator.h>


class PolarSurfaceGenerator : public MeshGenerator
{
public:
    explicit PolarSurfaceGenerator();
    virtual ~PolarSurfaceGenerator() override;

    virtual QSharedPointer<Mesh> generate() override;

    QSharedPointer<ASTNode> &function_r();
    const QSharedPointer<ASTNode> &function_r() const;

    unsigned int &lattitude_subdivision_level();
    unsigned int &longitude_subdivision_level();

    const unsigned int &lattitude_subdivision_level() const;
    const unsigned int &longitude_subdivision_level() const;

private:
    QSharedPointer<ASTNode> m_function_r;

    unsigned int m_lattitude_subdivision_level;
    unsigned int m_longitude_subdivision_level;

    QSharedPointer<Mesh> generateSegment(float left_longitude_angle, float right_longitude_angle);
    QSharedPointer<QVector3D> generateVertex(float longitude_angle, float lattitude_angle);
};

#endif // POLAR_SURFACE_GENERATOR_H

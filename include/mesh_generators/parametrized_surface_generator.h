#ifndef PARAMETRIZED_SURFACE_GENERATOR_H
#define PARAMETRIZED_SURFACE_GENERATOR_H

#include <ast_node.h>
#include <mesh_generator.h>


class ParametrizedSurfaceGenerator : public MeshGenerator {
public:
    explicit ParametrizedSurfaceGenerator();
    virtual ~ParametrizedSurfaceGenerator() override;

    virtual QSharedPointer<Mesh> generate() override;

    QSharedPointer<ASTNode> &function_x();
    QSharedPointer<ASTNode> &function_y();
    QSharedPointer<ASTNode> &function_z();

    unsigned int &s_subdivision_level();
    unsigned int &t_subdivision_level();

    float &min_s();
    float &max_s();
    float &min_t();
    float &max_t();

    const QSharedPointer<ASTNode> &function_x() const;
    const QSharedPointer<ASTNode> &function_y() const;
    const QSharedPointer<ASTNode> &function_z() const;

    const unsigned int &s_subdivision_level() const;
    const unsigned int &t_subdivision_level() const;

    const float &min_s() const;
    const float &max_s() const;
    const float &min_t() const;
    const float &max_t() const;


private:
    QSharedPointer<ASTNode> m_function_x;
    QSharedPointer<ASTNode> m_function_y;
    QSharedPointer<ASTNode> m_function_z;

    unsigned int m_s_subdivision_level;
    unsigned int m_t_subdivision_level;

    float m_min_s;
    float m_max_s;
    float m_min_t;
    float m_max_t;
};

#endif // PARAMETRIZED_SURFACE_GENERATOR_H

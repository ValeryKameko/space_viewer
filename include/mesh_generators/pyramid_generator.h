#ifndef PYRAMID_GENERATOR_H
#define PYRAMID_GENERATOR_H

#include "mesh.h"
#include "mesh_generator.h"
#include <QSharedPointer>
#include <QVector3D>

class PyramidGenerator : public MeshGenerator {
public:
    explicit PyramidGenerator(unsigned int count = 2, float height = 1.0, float radius = 1.0);
    virtual ~PyramidGenerator() override;

    virtual QSharedPointer<Mesh> generate() override;

    const float &radius() const;
    float &radius();

    const float &height() const;
    float &height();

    const unsigned int &count() const;
    unsigned int &count();
private:
    unsigned int m_count;
    float m_height;
    float m_radius;
    QVector3D generateVertex(float angle);
};


#endif // PYRAMID_GENERATOR_H

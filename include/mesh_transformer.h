#ifndef MESH_TRANSFORMER_H
#define MESH_TRANSFORMER_H

#include <QSharedPointer>
#include "mesh.h"

class MeshTransformer {
public:
    explicit MeshTransformer();
    virtual ~MeshTransformer();

    virtual QSharedPointer<Mesh> transform(const QSharedPointer<Mesh> &mesh) = 0;
};

#endif // MESH_TRANSFORMER_H

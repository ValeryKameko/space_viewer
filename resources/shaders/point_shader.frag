#version 430

uniform vec4 color;
out vec4 frag_color;

void main()
{
    vec2 current_coordinates = 2 * gl_PointCoord - 1.0;
    if (dot(current_coordinates, current_coordinates) > 1.0)
        discard;
    frag_color = color;
}

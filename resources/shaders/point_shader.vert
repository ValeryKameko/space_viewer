#version 430

in vec3 vertex_position;
uniform mat4 matrix;
uniform float point_size;
out float frag_point_size;

void main()
{
    gl_Position = matrix * vec4(vertex_position, 1.0);
    gl_PointSize = point_size;
}
